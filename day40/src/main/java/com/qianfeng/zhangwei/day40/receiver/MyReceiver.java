package com.qianfeng.zhangwei.day40.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.qianfeng.zhangwei.day40.JpushMsgActivity;

import cn.jpush.android.api.JPushInterface;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：MyReceiver
 * @创 建人：zhangwei
 * @创建时间：2015-06-29 16 : 09
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(JPushInterface.ACTION_REGISTRATION_ID)) {
            int registId = intent.getIntExtra(JPushInterface.EXTRA_REGISTRATION_ID, 0);
        } else if (intent.getAction().equals(JPushInterface.ACTION_MESSAGE_RECEIVED)) {
            String title = intent.getStringExtra(JPushInterface.EXTRA_TITLE);
            String content = intent.getStringExtra(JPushInterface.EXTRA_MESSAGE);
            String msgId = intent.getStringExtra(JPushInterface.EXTRA_MSG_ID);
        }else  if (intent.getAction().equals(JPushInterface.ACTION_NOTIFICATION_RECEIVED)){
            String title = intent.getStringExtra(JPushInterface.EXTRA_NOTIFICATION_TITLE);
            String content = intent.getStringExtra(JPushInterface.EXTRA_ALERT);
            String msgId  = intent.getStringExtra(JPushInterface.EXTRA_MSG_ID);

        }else if (intent.getAction().equals(JPushInterface.ACTION_NOTIFICATION_OPENED)){
            String content = intent.getStringExtra(JPushInterface.EXTRA_ALERT);
            String title = intent.getStringExtra(JPushInterface.EXTRA_NOTIFICATION_TITLE);
            Intent startIntent = new Intent(context, JpushMsgActivity.class);
            if (!TextUtils.isEmpty(content)){
                startIntent.putExtra("content",content);
            }
            if (!TextUtils.isEmpty(title)){
                startIntent.putExtra("title",title);
            }

            startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(startIntent);
        }


    }
}
