package com.qianfeng.zhangwei.day40;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.HashMap;

import cn.jpush.android.api.JPushInterface;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;

/**
 *
 *
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener, PlatformActionListener {
    private Button qqBtn;
    private Button sinaWeiboBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        qqBtn = (Button) this.findViewById(R.id.qq_btn);
        sinaWeiboBtn = (Button) this.findViewById(R.id.sina_btn);
        qqBtn.setOnClickListener(this);
        sinaWeiboBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Platform platform = null;
        switch (v.getId()) {
            case R.id.qq_btn:
                platform = ShareSDK.getPlatform(QQ.NAME);
                break;
            case R.id.sina_btn:
                platform = ShareSDK.getPlatform(SinaWeibo.NAME);
                break;
        }
        authorize(platform);
    }

    private void authorize(Platform plat) {
        if (plat == null) {
            return;
        }
        //判断指定平台是否已经完成授权
        if (plat.isValid()) {
            PlatformDb db = plat.getDb();
            String userId = db.getUserId();
            String name = db.getUserName();
//            String token = db.getTokenSecret();
            String iamgePath = db.getUserIcon();

            if (userId != null) {
//                login(plat.getName(), userId, null);
                return;
            }
        } else {
            plat.setPlatformActionListener(this);
            // true不使用SSO授权，false使用SSO授权
            plat.SSOSetting(true);
            //获取用户资料
            plat.showUser(null);
        }

    }

    /**
     * 登陆自己服务器的用户系统
     *
     * @param platName
     * @param userId
     */
    private void login(String platName, String userId) {
        new Thread(new Runnable() {
            @Override
            public void run() {

            }
        });
    }

    @Override
    public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
        Log.e("hashMap", hashMap.toString());
        String name = (String) hashMap.get("name");
        String id = (String) hashMap.get("id");
        //去自己后台的用户系统认证
//        login();

    }

    @Override
    public void onError(Platform platform, int i, Throwable throwable) {

    }

    @Override
    public void onCancel(Platform platform, int i) {

    }


    @Override
    protected void onPause() {
        super.onPause();
        JPushInterface.onPause(getApplicationContext());
    }


    @Override
    protected void onResume() {
        super.onResume();
        JPushInterface.onResume(getApplicationContext());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
