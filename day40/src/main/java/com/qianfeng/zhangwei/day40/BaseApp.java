package com.qianfeng.zhangwei.day40;

import android.app.Application;
import android.app.Notification;

import cn.jpush.android.api.BasicPushNotificationBuilder;
import cn.jpush.android.api.CustomPushNotificationBuilder;
import cn.jpush.android.api.JPushInterface;
import cn.sharesdk.framework.ShareSDK;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：BaseApp
 * @创 建人：zhangwei
 * @创建时间：2015-06-29 15 : 12
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class BaseApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        //初始化sharesdk第三方Sdk
        ShareSDK.initSDK(this);
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        /**
         * 上下文
         */
        CustomPushNotificationBuilder builder = new CustomPushNotificationBuilder(this, R.layout.customer_notitfication_layout, R.id.icon,
                R.id.title,
                R.id.text);
        JPushInterface.setDefaultPushNotificationBuilder(builder);

        JPushInterface.setPushNotificationBuilder(1, builder);

        BasicPushNotificationBuilder builder1 = new BasicPushNotificationBuilder(this);


        builder1.statusBarDrawable = R.mipmap.ic_launcher;
        builder1.notificationFlags = Notification.DEFAULT_SOUND;

        JPushInterface.setPushNotificationBuilder(2, builder1);
    }


}
