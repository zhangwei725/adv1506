package com.qianfeng.zhangwei.day46;

import android.os.Process;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.qianfeng.zhangwei.day46.adapter.TabVpiAdapter;
import com.qianfeng.zhangwei.day46.fragment.ExpListViewFragment_;
import com.qianfeng.zhangwei.day46.fragment.XmppFragment_;
import com.qianfeng.zhangwei.day46.utils.UpdateManger;
import com.viewpagerindicator.TabPageIndicator;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 *
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    @ViewById(R.id.main_tabi)
    TabPageIndicator tabPageIndicator;
    @ViewById(R.id.main_viewPager)
    ViewPager viewPager;
    private ArrayList<Fragment> fragments = new ArrayList<>();

    @Override
    public void initView() {
        UpdateManger.getInstance(this).update();



        fragments.add(new ExpListViewFragment_());
        fragments.add(new XmppFragment_());
        TabVpiAdapter adapter = new TabVpiAdapter(fragments, getSupportFragmentManager(), new String[]{"ExpListView", "XMPP"});
        viewPager.setAdapter(adapter);
        tabPageIndicator.setViewPager(viewPager);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        android.os.Process.killProcess(Process.myPid());
    }
}
