package com.qianfeng.zhangwei.day46.fragment;

import android.os.Handler;
import android.os.Message;
import android.provider.SyncStateContract;

import com.qianfeng.zhangwei.day46.R;

import org.androidannotations.annotations.EFragment;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;


/**
 * 第一步导入jar
 * 第二步 创建连接
 */
@EFragment(R.layout.fragment_xmpp)
public class XmppFragment extends BaseFragment {
    private static final String SERVER_HOST = "127.0.0.1";
    private static final String CLIENT_NAME = "iphone 6 plus";
    private static final int SERVER_PORT = 5222;

    private XMPPConnection connection;


    private static Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    //处理消息 连接服务器成功
                    break;
                case -1:
                    ///亲 网络太坑!换个号试试
                    break;
                case -2:
                    //登陆成功跳转好友界面
                    break;
                case -3:
                    ///登陆失败
                    break;


            }
        }
    };

    private static final String msgUrl = SyncStateContract.Constants.CONTENT_DIRECTORY;


    @Override
    public void initView() {
        super.initView();
        connectionServer();
        login("admin", "123456");
    }

    @Override
    public void initData() {
        super.initData();

    }


    /**
     * 连接服务器
     */
    private void connectionServer() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                Message message = handler.obtainMessage();

                int index = 0;
                ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration(SERVER_HOST, SERVER_PORT, CLIENT_NAME);
//        connectionConfiguration.setDebuggerEnabled(false);
                connectionConfiguration.setSASLAuthenticationEnabled(false);
                connection = new XMPPConnection(connectionConfiguration);
                try {
                    connection.connect();
                } catch (XMPPException e) {
                    e.printStackTrace();
                    index = -1;
                }
                message.what = index;
                handler.sendMessage(message);


            }
        }).start();
    }

    /**
     * -2登陆成功  -3登陆失败
     *
     * @param name
     * @param pwd
     */
    private void login(final String name, final String pwd) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                int index = -2;
                if (connection != null) {
                    try {
                        connection.login(name, pwd);
                    } catch (XMPPException e) {
                        e.printStackTrace();
                        index = -3;
                    }
                }
                handler.sendEmptyMessage(index);
            }
        }).start();
    }




    /**
     * 创建聊天
     */
    private void createChat() {
        ChatManager chatManager = connection.getChatManager();

        chatManager.createChat("", new MessageListener() {
            @Override
            public void processMessage(Chat chat, org.jivesoftware.smack.packet.Message message) {

            }
        });


    }


}
