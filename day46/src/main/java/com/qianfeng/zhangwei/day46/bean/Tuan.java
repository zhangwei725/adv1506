package com.qianfeng.zhangwei.day46.bean;

import java.io.Serializable;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：Tuan
 * @创 建人：zhangwei
 * @创建时间：2015-07-03 09 : 27
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class Tuan implements Serializable {

    private String brand_name;

    private String image;

    public String getBrand_name() {
        return brand_name;
    }

    public void setBrand_name(String brand_name) {
        this.brand_name = brand_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
