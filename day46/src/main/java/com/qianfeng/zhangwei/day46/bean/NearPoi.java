package com.qianfeng.zhangwei.day46.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：NearPoi
 * @创 建人：zhangwei
 * @创建时间：2015-07-03 09 : 26
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class NearPoi  implements Serializable{
    private List<Tuan> tuan_list;
    private String bizarea_title;
    private String poi_id;

    private String poi_image;
    private int tuan_num;
    private String poi_name;

    public List<Tuan> getTuan_list() {
        return tuan_list;
    }

    public void setTuan_list(List<Tuan> tuan_list) {
        this.tuan_list = tuan_list;
    }

    public String getBizarea_title() {
        return bizarea_title;
    }

    public void setBizarea_title(String bizarea_title) {
        this.bizarea_title = bizarea_title;
    }

    public String getPoi_id() {
        return poi_id;
    }

    public void setPoi_id(String poi_id) {
        this.poi_id = poi_id;
    }

    public String getPoi_image() {
        return poi_image;
    }

    public void setPoi_image(String poi_image) {
        this.poi_image = poi_image;
    }

    public int getTuan_num() {
        return tuan_num;
    }

    public void setTuan_num(int tuan_num) {
        this.tuan_num = tuan_num;
    }

    public String getPoi_name() {
        return poi_name;
    }

    public void setPoi_name(String poi_name) {
        this.poi_name = poi_name;
    }
}
