package com.qianfeng.zhangwei.day46.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.qianfeng.zhangwei.day46.R;
import com.qianfeng.zhangwei.day46.bean.NearPoi;
import com.qianfeng.zhangwei.day46.bean.Tuan;

import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：MyExpandabeListView
 * @创 建人：zhangwei
 * @创建时间：2015-07-03 09 : 20
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class MyExpandabeAdapter extends BaseExpandableListAdapter {
    private List<NearPoi> list;
    private LayoutInflater inflater;

    public MyExpandabeAdapter(List<NearPoi> list, Context context) {
        this.list = list;
        inflater = LayoutInflater.from(context);
    }


    /**
     * 组的长度
     *
     * @return
     */
    @Override
    public int getGroupCount() {
        return list.size();
    }

    /**
     * 获得子项的长度
     *
     * @param groupPosition
     * @return
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return list.get(groupPosition).getTuan_list().size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GroupViewHolder gvh = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_near_group_layout, parent, false);
            gvh = new GroupViewHolder(convertView);
            convertView.setTag(gvh);
        } else {
            gvh = (GroupViewHolder) convertView.getTag();
        }
        gvh.titile.setText(list.get(groupPosition).getPoi_name());
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ChildeViewHolder cvh = null;

        Tuan tuan = list.get(groupPosition).getTuan_list().get(childPosition);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_near_child_layout, parent, false);
            cvh = new ChildeViewHolder(convertView);
            convertView.setTag(cvh);
        } else {
            cvh = (ChildeViewHolder) convertView.getTag();
        }
        cvh.title.setText(tuan.getBrand_name());
        return convertView;
    }


    private static class GroupViewHolder {
        TextView titile;

        public GroupViewHolder(View convertView) {
            titile = (TextView) convertView.findViewById(R.id.item_near_group_name);
        }
    }


    private static class ChildeViewHolder {
        private ImageView img;
        private TextView title;

        public ChildeViewHolder(View convertView) {
            this.img = (ImageView) convertView.findViewById(R.id.item_near_childe_img);
            title = (TextView) convertView.findViewById(R.id.item_near_childe_title);
        }
    }


    /**
     * -----------------------------
     */

    @Override
    public Object getGroup(int groupPosition) {
        return null;
    }


    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }
}
