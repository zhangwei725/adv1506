package com.qianfeng.zhangwei.day46;

import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

@EActivity
public class BaseActivity extends AppCompatActivity {

    /**
     *
     */
    @AfterViews
    public void init() {
        initView();
        initData();

    }

    /**
     *
     */
    @AfterInject
    public void inject() {

    }


    public void initView() {

    }

    public void initData() {

    }


}
