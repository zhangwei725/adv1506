package com.qianfeng.zhangwei.day46.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：PoiBean
 * @创 建人：zhangwei
 * @创建时间：2015-07-03 14 : 25
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class PoiBean implements Serializable {
    private List <NearPoi> poi_list;

    public List<NearPoi> getPoi_list() {
        return poi_list;
    }

    public void setPoi_list(List<NearPoi> poi_list) {
        this.poi_list = poi_list;
    }
}
