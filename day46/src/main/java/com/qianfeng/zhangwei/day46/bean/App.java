package com.qianfeng.zhangwei.day46.bean;

import java.io.Serializable;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：App
 * @创 建人：zhangwei
 * @创建时间：2015-07-03 10 : 07
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class App<T> implements Serializable {

    private int timestamp;
    private int serverlogid;
    private int cached;
    private int errno;
    private int serverstatus;
    private String errmsg;
    private String msg;
    private PoiBean data;

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public void setServerlogid(int serverlogid) {
        this.serverlogid = serverlogid;
    }

    public void setCached(int cached) {
        this.cached = cached;
    }

    public void setErrno(int errno) {
        this.errno = errno;
    }

    public void setServerstatus(int serverstatus) {
        this.serverstatus = serverstatus;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public int getServerlogid() {
        return serverlogid;
    }

    public int getCached() {
        return cached;
    }

    public int getErrno() {
        return errno;
    }


    public int getServerstatus() {
        return serverstatus;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public String getMsg() {
        return msg;
    }

    public class DataEntity {
    }

    public PoiBean getData() {
        return data;
    }

    public void setData(PoiBean data) {
        this.data = data;
    }
}
