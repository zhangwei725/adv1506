package com.qianfeng.zhangwei.day46.fragment;

import android.app.Activity;
import android.support.v4.app.Fragment;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;


@EFragment
public class BaseFragment extends Fragment {

    public Activity activity;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @AfterViews
    public void init() {
        initView();
        initData();
    }

    public void initView() {
    }

    public void initData() {

    }

    @AfterInject
    public void inject() {

    }

}
