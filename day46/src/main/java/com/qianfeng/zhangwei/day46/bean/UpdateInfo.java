package com.qianfeng.zhangwei.day46.bean;

import android.text.TextUtils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：UpdateInfo
 * @创 建人：zhangwei
 * @创建时间：2015-07-04 11 : 24
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */

public class UpdateInfo implements Serializable {
    private String appName = null;
    private String appDescription = null;
    private String packageName = null;
    private String versionCode = null;
    private String versionName = null;
    private boolean forceUpdate = false;
    private boolean autoUpdate = false;
    private String apkUrl = null;
    private Map<String, String> updateTips = null;


    public final static String TAG_UPDATE_INFO = "updateInfo";
    public final static String TAG_APP_NAME = "appName";
    public final static String TAG_APP_DESCRIPTION = "appDescription";
    public final static String TAG_PACKAGE_NAME = "packageName";
    public final static String TAG_VERSION_CODE = "versionCode";
    public final static String TAG_VERSION_NAME = "versionName";
    public final static String TAG_FORCE_UPDATE = "forceUpdate";
    public final static String TAG_AUTO_UPDATE = "autoUpdate";
    public final static String TAG_APK_URL = "apkUrl";
    public final static String TAG_UPDATE_TIPS = "updateTips";

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppDescription() {
        return appDescription;
    }

    public void setAppDescription(String appDescription) {
        this.appDescription = appDescription;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public boolean isForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public boolean isAutoUpdate() {
        return autoUpdate;
    }

    public void setAutoUpdate(boolean autoUpdate) {
        this.autoUpdate = autoUpdate;
    }

    public String getApkUrl() {
        return apkUrl;
    }

    public void setApkUrl(String apkUrl) {
        this.apkUrl = apkUrl;
    }

    public Map<String, String> getUpdateTips() {
        return updateTips;
    }

    public void setUpdateTips(Map<String, String> updateTips) {
        this.updateTips = updateTips;
    }

    public static UpdateInfo parseXMl(InputStream inputStream) {

        UpdateInfo info = null;
        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();

            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(inputStream, "UTF-8");
            int eventType = xpp.getEventType();
            String currentTag = null;
            while (eventType != XmlPullParser.END_DOCUMENT) {
                switch (eventType) {
                    case XmlPullParser.START_DOCUMENT:
                        break;
                    case XmlPullParser.START_TAG:
                        currentTag = xpp.getName();
                        if (currentTag.equals(TAG_UPDATE_INFO)) {
                            info = new UpdateInfo();
                        } else if (currentTag.equals(TAG_APP_NAME)) {
                            if (info != null) {
                                info.setAppName(xpp.nextText());
                            }
                        } else if (currentTag.equals(TAG_APP_DESCRIPTION)) {
                            if (info != null) {
                                info.setAppDescription(xpp.nextText());
                            }
                        } else if (currentTag.equals(TAG_PACKAGE_NAME)) {
                            if (info != null) {
                                info.setPackageName(xpp.nextText());
                            }
                        } else if (currentTag.equals(TAG_VERSION_CODE)) {
                            if (info != null) {
                                info.setVersionCode(xpp.nextText());
                            }
                        } else if (currentTag.equals(TAG_VERSION_NAME)) {
                            if (info != null) {
                                info.setVersionName(xpp.nextText());
                            }
                        } else if (currentTag.equals(TAG_FORCE_UPDATE)) {
                            if (info != null) {
                                String str = xpp.nextText();
                                info.setForceUpdate((!TextUtils.isEmpty(str) && str.equalsIgnoreCase("true")) ? true : false);
                            }
                        } else if (currentTag.equals(TAG_AUTO_UPDATE)) {
                            if (info != null) {
                                String str = xpp.nextText();
                                info.setForceUpdate((!TextUtils.isEmpty(str) && str.equalsIgnoreCase("true")) ? true : false);
                            }
                        } else if (currentTag.equals(TAG_APK_URL)) {
                            if (info != null) {
                                info.setApkUrl(xpp.nextText());
                            }
                        } else if (currentTag.equals(TAG_UPDATE_TIPS)) {
                            Map<String, String> map = parseUpdateTips(xpp);
                            if (info != null) {
                                info.setUpdateTips(map);
                            }
                        }
                        break;
                    case XmlPullParser.END_TAG:
                        break;
                    case XmlPullParser.TEXT:
                        break;
                    default:
                        break;
                }
                eventType = xpp.next();
            }

            return info;


        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return info;


    }

    private static Map<String, String> parseUpdateTips(XmlPullParser xpp) throws XmlPullParserException, IOException {
        Map<String, String> updateTips = new HashMap<String, String>();
        String currentTag = null;
        String currentValue = null;

        int eventType = xpp.getEventType();
        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    break;
                case XmlPullParser.START_TAG:
                    currentTag = xpp.getName();
                    if (currentTag.equals(TAG_UPDATE_TIPS)) {
                        updateTips = new HashMap<String, String>();
                    } else {
                        currentValue = xpp.nextText();
                        updateTips.put(currentTag, currentValue);
                    }
                    break;
                case XmlPullParser.END_TAG:
                    break;
                case XmlPullParser.TEXT:
                    break;
                default:
                    break;
            }
            eventType = xpp.next();
        }
        return updateTips;
    }

}
