package com.qianfeng.zhangwei.day46.fragment;


import android.view.View;
import android.widget.ExpandableListView;

import com.alibaba.fastjson.TypeReference;
import com.qianfeng.zhangwei.day46.R;
import com.qianfeng.zhangwei.day46.adapter.MyExpandabeAdapter;
import com.qianfeng.zhangwei.day46.bean.App;
import com.qianfeng.zhangwei.day46.bean.NearPoi;
import com.qianfeng.zhangwei.day46.constant.UrlConstants;
import com.qianfeng.zhangwei.day46.utils.XUtilsTools;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;


@EFragment(R.layout.fragment_exp_list_view)
public class ExpListViewFragment extends BaseFragment implements ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener {
    @ViewById(R.id.fm_expListView)
    ExpandableListView expListView;

    private MyExpandabeAdapter expandabeAdapter;
    private List<NearPoi> nearPois = new ArrayList<NearPoi>();

    @Override
    public void initView() {
        expandabeAdapter = new MyExpandabeAdapter(nearPois, getActivity());
        expListView.setOnGroupClickListener(this);
        expListView.setAdapter(expandabeAdapter);


    }

    @Override
    public void initData() {
        XUtilsTools.getInstance().get(UrlConstants.NEAR_URL, expandabeAdapter,expListView ,nearPois, new TypeReference<App<NearPoi>>() {
        });
    }




    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        return true;
    }
}
