package com.qianfeng.zhangwei.easemob.activity;


import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qianfeng.zhangwei.easemob.R;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends BaseActivity implements Animation.AnimationListener {
    @ViewById(R.id.splash_root)
    RelativeLayout rootLayout;
    @ViewById(R.id.tv_version)
    TextView versionText;

    public void initView() {
        versionText.setText(getVersion());
        AnimationSet animationSet = new AnimationSet(true);
        AlphaAnimation animation = new AlphaAnimation(0.3f, 1.0f);
        animation.setDuration(1500);
        animationSet.addAnimation(animation);
        animationSet.setAnimationListener(this);
        rootLayout.startAnimation(animationSet);
    }

    /**
     * 获取当前应用程序的版本号
     */
    private String getVersion() {
        String st = getResources().getString(R.string.Version_number_is_wrong);
        PackageManager pm = getPackageManager();
        try {
            PackageInfo packinfo = pm.getPackageInfo(getPackageName(), 0);
            String version = packinfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return st;
        }
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        startActivity(new Intent(this, LoginActivity_.class));
        this.finish();
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }
}
