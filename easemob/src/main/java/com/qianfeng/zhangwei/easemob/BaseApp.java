package com.qianfeng.zhangwei.easemob;

import android.app.Application;

import com.easemob.chat.EMChat;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.easemob
 * @类 名： BaseApp
 * @创 建人： zhangwei
 * @创建时间：2015-07-12 10:42
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */

public class BaseApp extends Application {
    private static BaseApp app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        //初始化第三方功能-环信sdk
        EMChat.getInstance().init(this);//封装常识
//        EMChat.getInstance().setDebugMode(true);


    }

    public static BaseApp getInstance() {
        return app;
    }


}
