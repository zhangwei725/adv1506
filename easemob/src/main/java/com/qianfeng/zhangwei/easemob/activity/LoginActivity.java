package com.qianfeng.zhangwei.easemob.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.easemob.EMCallBack;
import com.easemob.chat.EMChatManager;
import com.qianfeng.zhangwei.easemob.R;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import om.qianfeng.lib.verification.Rule;
import om.qianfeng.lib.verification.Validator;
import om.qianfeng.lib.verification.annotation.Password;
import om.qianfeng.lib.verification.annotation.Required;

/**
 * 登陆
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity implements Validator.ValidationListener, EMCallBack {
    @ViewById(R.id.username)
    @Required(message = "不能为空", order = 0)
    EditText usernameEditText;
    @ViewById(R.id.password)
    @Password(minLength = 6, order = 0)
    EditText passwordEditText;
    @ViewById
    Button register;
    @ViewById
    Button login;
    Validator validator;
    String pwd;
    String userName;

    @Override
    public void onValidationSucceeded() {
        userName = usernameEditText.getText().toString().trim();
        pwd = passwordEditText.getText().toString().trim();
        if (!TextUtils.isEmpty(userName) && !TextUtils.isEmpty(pwd)) {
            //登陆
            EMChatManager.getInstance().login(userName, pwd, this);
            EMChatManager.getInstance().loadAllConversations();
        }
    }

    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {

    }

    @Click({R.id.register, R.id.login})
    public void OnClickListener(View view) {
        switch (view.getId()) {
            case R.id.register:
                startActivity(new Intent(this, RegisterActivity_.class));
                this.finish();
                break;
            case R.id.login:
                validator = new Validator(this);
                validator.setValidationListener(this);
                validator.validate();
//                EMChatManager.getInstance().login(userName, pwd, this);
                break;
        }

    }

    @Override
    public void onSuccess() {
        startActivity(new Intent(this, MainActivity_.class));
        this.finish();
    }

    @Override
    public void onError(int i, String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProgress(int i, String s) {

    }
}
