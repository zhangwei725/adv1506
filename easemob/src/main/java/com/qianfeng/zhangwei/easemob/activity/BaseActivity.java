package com.qianfeng.zhangwei.easemob.activity;

import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

@EActivity
public class BaseActivity extends AppCompatActivity {

    @AfterViews
    public void init() {
        initView();
        initData();
    }


    public void initView() {

    }


    public void initData() {

    }
}
