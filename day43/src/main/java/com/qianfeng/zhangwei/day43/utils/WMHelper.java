package com.qianfeng.zhangwei.day43.utils;

import android.content.Context;
import android.graphics.PixelFormat;
import android.view.Gravity;
import android.view.WindowManager;

import com.qianfeng.zhangwei.day43.widget.FloatView;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：WMHelper
 * @创 建人：zhangwei
 * @创建时间：2015-07-01 17 : 29
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class WMHelper {


    private static Context context;

    private static WMHelper wmHelper;

    private FloatView floatView;


    private WindowManager wm;


    private WindowManager.LayoutParams params;

    public static void init(Context context) {
        WMHelper.context = context;
    }


    private WMHelper() {
        if (wm == null) {
            wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        }
        if (floatView == null) {
            floatView = new FloatView(context);
        }
    }

    public static WMHelper getInstance() {
        if (wmHelper == null) {
            wmHelper = new WMHelper();
        }
        return wmHelper;
    }


    public void showFloatView() {
        if (params == null) {
            params = new WindowManager.LayoutParams();
            params.width = WindowManager.LayoutParams.WRAP_CONTENT;
            params.height = WindowManager.LayoutParams.WRAP_CONTENT;
            //初始化wm的x y的坐标  跟params.gravity的设置有关
            params.x = 200;
            params.y = 200;
            //设置wm的显示权限
            params.type = WindowManager.LayoutParams.TYPE_PHONE;
            //设置wm不自动获取焦点
            params.flags = WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
            //设置wm的背景颜色透明
            params.format = PixelFormat.RGBA_8888;
            params.gravity = Gravity.TOP | Gravity.LEFT;
        }
        floatView.setWm(wm);
        floatView.setParams(params);
        wm.addView(floatView, params);
    }

    public void dissmiss() {
        if (wm != null) {
            wm.removeView(floatView);
            wm = null;
        }
    }

}
