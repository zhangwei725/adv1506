package com.qianfeng.zhangwei.day43;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.qianfeng.zhangwei.day43.utils.WMHelper;
import com.qianfeng.zhangwei.day43.wm.WmanagerActivity;

public class MainActivity extends AppCompatActivity {
    private Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WMHelper.init(getApplicationContext());
        btn = (Button) this.findViewById(R.id.float_btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.startActivity(new Intent(MainActivity.this, WmanagerActivity.class));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        WMHelper.getInstance().dissmiss();
    }
}
