package com.qianfeng.zhangwei.day43.wm;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.qianfeng.zhangwei.day43.R;
import com.qianfeng.zhangwei.day43.utils.WMHelper;

/**
 * add(View view  LayoutParams)
 * remove(view)
 * upadte(View view  LayoutParams)
 */
public class WmanagerActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wmanager);
        WMHelper.getInstance().showFloatView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }


}
