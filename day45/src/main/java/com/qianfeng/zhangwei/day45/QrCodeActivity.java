package com.qianfeng.zhangwei.day45;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.zbar.lib.CaptureActivity;

public class QrCodeActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 223;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_code);
        startActivityForResult(new Intent(this, CaptureActivity.class), REQUEST_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE) {
            String title = data.getExtras().getString("result");
            if (!TextUtils.isEmpty(title)){
                                
            }
        }


    }
}
