package com.qianfeng.zhangwei.day45;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.qianfeng.zhangwei.day45.adapter.BlueAdapter;
import com.qianfeng.zhangwei.day45.task.ServerThead;

import java.util.ArrayList;
import java.util.UUID;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {


    /**
     *
     * 代表本地的蓝牙设备
     *
     */
    private BluetoothAdapter bluetoothAdapter;


    private Button openBtn;

    private Button disBtn;


    private Button searchBtn;

    private Button connectionBtn;

    private ArrayList<BluetoothDevice> devices = new ArrayList<>();

    private BlueAdapter adapter;

    private ListView listView;

    private UUID uuid = UUID.randomUUID();


    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(BluetoothDevice.ACTION_FOUND)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_NAME);
                devices.add(device);
                adapter.notifyDataSetChanged();
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        openBtn = (Button) this.findViewById(R.id.open_btn);
        disBtn = (Button) this.findViewById(R.id.dis_btn);
        searchBtn = (Button) this.findViewById(R.id.search_btn);
        connectionBtn = (Button) this.findViewById(R.id.connection_btn);
        openBtn.setOnClickListener(this);
        disBtn.setOnClickListener(this);
        searchBtn.setOnClickListener(this);
        connectionBtn.setOnClickListener(this);
        //实例化本地蓝牙设备
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();


        //listview


        listView = (ListView) this.findViewById(R.id.listview);
        adapter = new BlueAdapter(devices, this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
    }

    /**
     * 打开蓝牙
     */
    private void open() {
        /* 弹出界面  */
        if (bluetoothAdapter != null && !bluetoothAdapter.isEnabled()) {
            //隐式打开蓝牙
            bluetoothAdapter.enable();
            /**
             * 显式的打开蓝牙
             */
            Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivity(intent);
        }
    }


    /**
     * 设置蓝牙的搜搜的显示时间
     */
    private void setDis() {
        Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
        intent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
        startActivity(intent);
    }

    /**
     * 搜索蓝牙设备
     */
    private void searchBluetooth() {
        IntentFilter myIntentFilter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        //注册广播
        registerReceiver(receiver, myIntentFilter);
        bluetoothAdapter.startDiscovery();

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.open_btn:
                open();
                break;
            case R.id.dis_btn:
                setDis();
                break;
            case R.id.search_btn:
                searchBluetooth();
                break;
            case R.id.connection_btn:
                break;
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //停止搜索
        bluetoothAdapter.cancelDiscovery();
        //要链接的设备
        BluetoothDevice device = devices.get(position);
        new ServerThead(bluetoothAdapter, device).start();
    }
}
