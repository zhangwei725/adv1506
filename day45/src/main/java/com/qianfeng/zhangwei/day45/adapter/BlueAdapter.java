package com.qianfeng.zhangwei.day45.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.qianfeng.zhangwei.day45.R;

import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：BlueAdapter
 * @创 建人：zhangwei
 * @创建时间：2015-07-02 09 : 46
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class BlueAdapter extends BaseAdapter {

    private List<BluetoothDevice> devices;

    private LayoutInflater inflater;


    public BlueAdapter(List<BluetoothDevice> devices, Context context) {
        this.devices = devices;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return devices.size();
    }

    @Override
    public Object getItem(int position) {
        return devices.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_bluetooth_layout, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.name.setText(devices.get(position).getName());
        holder.address.setText(devices.get(position).getAddress());
        return convertView;
    }

    private static class ViewHolder {
        private TextView name;
        private TextView address;


        public ViewHolder(View itemView) {
            this.name = (TextView) itemView.findViewById(R.id.item_name);
            this.address = (TextView) itemView.findViewById(R.id.item_address);
        }
    }

}
