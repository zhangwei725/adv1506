package com.qianfeng.zhangwei.day45.task;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.UUID;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：ServerThead
 * @创 建人：zhangwei
 * @创建时间：2015-07-02 10 : 54
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class ServerThead extends Thread {
    private BluetoothAdapter adapter;
    private BluetoothDevice device;

    private UUID uuid = UUID.fromString("00001106-0000-1000-8000-00805F9B34FB");


    public ServerThead(BluetoothAdapter adapter, BluetoothDevice device) {
        this.adapter = adapter;
        this.device = device;
    }


    @Override
    public void run() {
        super.run();
        try {
            BluetoothServerSocket serverSocket = adapter.listenUsingRfcommWithServiceRecord(device.getName(), uuid);
            BluetoothSocket socket = serverSocket.accept();
            socket.connect();
            OutputStream outputStream = socket.getOutputStream();
            FileInputStream fis = new FileInputStream("sdk/img/11111.png");
            byte[] data = new byte[2 * 1024];
            int count = 0;
            while ((count = fis.read(data)) != -1) {
                outputStream.write(data, 0, count);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
