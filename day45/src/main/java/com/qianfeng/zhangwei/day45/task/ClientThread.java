package com.qianfeng.zhangwei.day45.task;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：ServerThread
 * @创 建人：zhangwei
 * @创建时间：2015-07-02 10 : 47
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class ClientThread extends Thread {
    private BluetoothDevice device;
    private UUID uuid = UUID.fromString("00001106-0000-1000-8000-00805F9B34FB");

    public ClientThread(BluetoothDevice device) {
        this.device = device;
    }

    @Override
    public void run() {
        super.run();
        try {
            BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(uuid);
            if (!socket.isConnected()) {
                socket.connect();
            }
            InputStream inputStream = socket.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
