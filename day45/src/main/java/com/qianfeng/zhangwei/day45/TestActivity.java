package com.qianfeng.zhangwei.day45;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.qianfeng.zhangwei.day45.utils.AesUtil;
import com.qianfeng.zhangwei.day45.utils.Base64;

/**
 * 加密
 */
public class TestActivity extends AppCompatActivity {
    private String text = "微笑向暖,安之若素";

    private TextView textView;

    private String desKey = "31313131313131313131313";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        textView = (TextView) findViewById(R.id.textView);
        //Md5加密
//        String md5Str = new String(MD5.hexStringToByte(text));
//        textView.setText(md5Str);
//        try {
//            //desutils
//            String desStr = DesUtils.encode(desKey, text);
//            textView.setText(desStr);
//            String decodeStr = DesUtils.decodeValue(desKey, desStr);
//            Log.e("encodeStr" ,decodeStr);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        String aesStr = AesUtil.encrypt(text);
        textView.setText(aesStr);
//      字符串
        String baseStr = Base64.encode(text.getBytes());
        String baseDecStr = new String(Base64.decode(baseStr));
    }

}
