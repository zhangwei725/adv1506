package com.qianfeng.zhangwei.day37.utils;

import android.app.Activity;
import android.nfc.cardemulation.OffHostApduService;
import android.view.View;

import com.qianfeng.zhangwei.day37.R;
import com.qianfeng.zhangwei.day37.annotation.ResInit;
import com.qianfeng.zhangwei.day37.annotation.ViewInit;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-24 15:29
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class ViewUtil {

    public static void ViewInit(Activity activity) {
        //第一步获得Class对象
        Class<?> cls = activity.getClass();
        //第二步或者类的所有属性
        Field[] fields = cls.getDeclaredFields();
        if (fields != null && fields.length > 0) {
            //遍历所有成员变量
            for (Field field : fields) {
                ViewInit viewInit = field.getAnnotation(ViewInit.class);
                if (viewInit != null) {
                    int id = viewInit.id();
                    //通过注解获得属性的id 然后通过id来初始化属性
                    View view = activity.findViewById(id);
                    if (view != null) {
                        try {
                            field.setAccessible(true);
                            //给成员变量赋值
                            field.set(activity, view);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                } else {
                    ResInit resInit = field.getAnnotation(ResInit.class);
                    if (resInit != null) {
                        try {
                            Object object = ResLoader.loadrRes(resInit.id(), activity, resInit.type());
                            field.setAccessible(true);
                            field.set(activity, object);
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
                Method[] methods = cls.getDeclaredMethods();
                if (methods != null && methods.length > 0) {
                    for (Method method : methods) {
//                        method.invoke(activity, new Object[]{})
                    }


                }


            }
        }


    }


}
