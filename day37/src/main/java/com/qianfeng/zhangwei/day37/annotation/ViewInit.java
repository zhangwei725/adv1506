package com.qianfeng.zhangwei.day37.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-24 15:39
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 * 其他使用的时候都要以key=value的形式出现
 */


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ViewInit {
    int id();
}
