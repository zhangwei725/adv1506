package com.qianfeng.zhangwei.day37.annotation;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-24 15:59
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public enum ResType {
    String, Drawable, Color
}
