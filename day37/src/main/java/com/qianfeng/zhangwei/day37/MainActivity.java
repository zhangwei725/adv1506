package com.qianfeng.zhangwei.day37;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.ResType;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ResInject;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.qianfeng.zhangwei.day37.annotation.ResInit;
import com.qianfeng.zhangwei.day37.annotation.ViewInit;
import com.qianfeng.zhangwei.day37.fragment.AnnotationFragment;

import org.w3c.dom.Text;

/**
 * ViewUtils的使用
 * 通过注解的方式初始化ui控件 ,事件监听, 资源文件
 * IOC 控制反转0220
 */
@ContentView(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    @ViewInit(id = R.id.textview)
    private TextView text;

    @ViewInject(R.id.btn)
    private Button btn;

    @ViewInject(R.id.img)
    private ImageView img;

    @ResInject(id = R.string.app_name, type = ResType.String)
    private String name;
    @ResInit(id = R.string.app_name,type= com.qianfeng.zhangwei.day37.annotation.ResType.Drawable)
    private Drawable drawable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AnnotationFragment()).commit();
            name = getResources().getString(R.string.app_name);
            drawable = getResources().getDrawable(R.mipmap.ic_launcher);




    }

    private void addHeadView(LayoutInflater inflater) {
        View view = inflater.inflate(R.layout.head_layout, null);
        HeadVIew headVIew = new HeadVIew(view);

    }

    private class HeadVIew {
        @ViewInject(R.id.head_text)
        TextView textView;
        @ViewInject(R.id.head_btn)
        Button button;

        public HeadVIew(View view) {
            ViewUtils.inject(this, view);
        }


        @OnClick(R.id.head_btn)
        public void onClick(View v) {

        }
    }


    @OnClick({R.id.btn, R.id.textview, R.id.img})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn:
                Toast.makeText(this, "按钮被点击", Toast.LENGTH_SHORT).show();
                break;
            case R.id.textview:
                Toast.makeText(this, "文本呗点击", Toast.LENGTH_SHORT).show();
                break;
            case R.id.img:
                Toast.makeText(this, "图片呗点击", Toast.LENGTH_SHORT).show();
                break;
        }


    }
}
