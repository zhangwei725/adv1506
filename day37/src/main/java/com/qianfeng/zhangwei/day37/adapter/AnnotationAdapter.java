package com.qianfeng.zhangwei.day37.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.qianfeng.zhangwei.day37.R;

import java.util.ArrayList;
import java.util.List;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-24 14:32
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class AnnotationAdapter extends BaseAdapter {
    private List<String> list = new ArrayList<>();

    @Override
    public int getCount() {
        return list!= null && !list.isEmpty() ? list.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }


    private static class ViewHolder {

        @ViewInject(R.id.item_text)
        TextView name;
        @ViewInject(R.id.item_img)
        ImageView img;

        public ViewHolder(View itemView) {
            ViewUtils.inject(this, itemView);

        }
    }

}
