package com.qianfeng.zhangwei.day37.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnItemClick;
import com.qianfeng.zhangwei.day37.R;

import java.util.ArrayList;
import java.util.List;


public class AnnotationFragment extends Fragment {
    @ViewInject(R.id.fragment_listview)
    private ListView listView;
    private Button btn;
    private ArrayList<String> list = new ArrayList<>();
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_annotation, container, false);


        ViewUtils.inject(this, view);
        for (int i = 0; i < 20; i++) {
            list.add("测试代码" + i);
        }
        listView.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, list));
        return view;
    }

    @OnItemClick(R.id.fragment_listview)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getActivity(), "++++++" + position, Toast.LENGTH_SHORT).show();
    }
}
