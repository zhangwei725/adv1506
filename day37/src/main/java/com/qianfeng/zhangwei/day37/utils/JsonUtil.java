package com.qianfeng.zhangwei.day37.utils;

import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Field;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-24 10:22
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class JsonUtil {
    /**
     * 获得类的所有属性给属性赋值
     *
     * @param json
     * @param cls
     * @return
     */
    public static Object fromJson(String json, Class<?> cls) throws Exception {
        Field[] files = cls.getDeclaredFields();
        JSONObject jsonObject = new JSONObject(json);
        Object object = cls.newInstance();
        if (files != null && files.length > 0) {
            for (Field field : files) {
                if (field != null) {
                    Class<?> typeCls = field.getType();
                    if (typeCls.getSimpleName().equals("String")) {
                        field.setAccessible(true);
                        field.set(object, jsonObject.get(field.getName()));
                    }

                }
            }
        }
        return object;
    }


}
