package com.qianfeng.zhangwei.day37.utils;

import android.content.Context;

import com.qianfeng.zhangwei.day37.annotation.ResType;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-24 16:07
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class ResLoader {

    public static Object loadrRes(int id, Context context, ResType type) {
        Object obj = null;
        switch (type) {
            case String:
                obj = context.getResources().getString(id);
                break;
            case Drawable:
                obj = context.getResources().getDrawable(id);
                break;


        }
        return obj;
    }


}
