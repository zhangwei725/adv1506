package com.qianfeng.zhangwei.vpidemo;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.qianfeng.zhangwei.vpidemo.adapter.TabVpiAdapter;
import com.qianfeng.zhangwei.vpidemo.fragment.TestFragment;
import com.viewpagerindicator.TabPageIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * 第一步在工作空间下的 build.gradle 添加maven库地址
 * 第二步在项目向的build.gradle 添加aar包
 * compile 'com.viewpagerindicator:library:2.4.1'
 *
 * 第三步 使用
 * 3.1> 在主题中添加vpi样式
 * 3.2> 在布局文件中声明控件
 * 3.3> 初始化控件
 */
public class MainActivity extends AppCompatActivity {
    private TabPageIndicator tabPageIndicator;
    private ViewPager viewPager;

    private TabVpiAdapter adapter;

    private String titles[] = {"测试1", "测试2", "测试3", "测试4", "测试4", "测试4", "测试4", "测试4"};

    private List<Fragment> fragmentList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentList.add(new TestFragment());
        fragmentList.add(new TestFragment());
        fragmentList.add(new TestFragment());
        fragmentList.add(new TestFragment());
        fragmentList.add(new TestFragment());
        fragmentList.add(new TestFragment());
        fragmentList.add(new TestFragment());
        fragmentList.add(new TestFragment());

        adapter = new TabVpiAdapter(fragmentList, getSupportFragmentManager(),titles);
        viewPager = (ViewPager) this.findViewById(R.id.main_viewpager);
        viewPager.setAdapter(adapter);
        tabPageIndicator = (TabPageIndicator) this.findViewById(R.id.main_vpi_tbi);
        tabPageIndicator.setViewPager(viewPager);
    }

}
