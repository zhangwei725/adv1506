package com.qianfeng.zhangwei.extras.eventbus;

import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.acitivity.BaseActivity;
import com.qianfeng.zhangwei.extras.bean.Params;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;

@EActivity(R.layout.activity_event_bus)
public class EventBusActivity extends BaseActivity {
    @ViewById(R.id.eb_btn)
    Button ebBtn;
    @ViewById(R.id.test_btn)
    Button testBtn;
    @ViewById
    TextView msg;

    private EventBus eventBus = EventBus.getDefault();


    @Override
    public void inject() {
        eventBus.register(this);
    }

    @Override
    public void initView() {
        getSupportFragmentManager().beginTransaction().replace(R.id.eb_container, new TestFragment_()).commit();
    }

    @Subscriber
    private void test(Params params) {
        msg.setText(params.getLang());
    }


    @Click(R.id.eb_btn)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.eb_btn:
                Intent intent = new Intent(this, PostActivity_.class);
                startActivity(intent);
                break;
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }
}
