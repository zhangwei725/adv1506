package com.qianfeng.zhangwei.extras.utils;

import com.qianfeng.zhangwei.extras.BaseApplication;
import com.qianfeng.zhangwei.extras.netstate.NetChangeObserver;
import com.qianfeng.zhangwei.extras.netstate.NetWorkUtil;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.utils
 * @类 名： NetChangeObserverUtils
 * @创 建人： zhangwei
 * @创建时间：2015-07-10 09:18
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class NetChangeObserverUtils extends NetChangeObserver {

    @Override
    public void onConnect(NetWorkUtil.NetType type) {
        super.onConnect(type);
        switch (type) {
            case WIFI:
                BaseApplication.getInstance().showToast("wifi网络已连接");
                break;
            case CMNET:
                BaseApplication.getInstance().showToast("中国移动2/3/4G网络已连接");
                break;
            case GNET_3:
                BaseApplication.getInstance().showToast("中国联通2/3/4G网络已连接");
                break;
            case CTNET:
                BaseApplication.getInstance().showToast("中国电影2/3/4G网络已连接");
                break;
            default:
                BaseApplication.getInstance().showToast("网络已连接");
                break;
        }

    }

    @Override
    public void onDisConnect() {
        super.onDisConnect();
        BaseApplication.getInstance().showToast("无网络连接");
    }
}
