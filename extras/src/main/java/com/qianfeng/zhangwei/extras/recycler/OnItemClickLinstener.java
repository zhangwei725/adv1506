package com.qianfeng.zhangwei.extras.recycler;

import android.view.View;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.extras.recycler
 * @类 名： OnItemClickLinstener
 * @创 建人： zhangwei
 * @创建时间：2015-07-06 14:40
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public interface OnItemClickLinstener {

    public void onItemClickLinstener(View itemView, int postion);

    public <T> void onItemClickLinstener(T t);

}
