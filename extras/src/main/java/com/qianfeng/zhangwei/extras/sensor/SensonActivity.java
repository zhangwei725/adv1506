package com.qianfeng.zhangwei.extras.sensor;

import android.content.Context;
import android.os.Vibrator;

import com.qianfeng.zhangwei.extras.BaseApplication;
import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.acitivity.BaseActivity;

import org.androidannotations.annotations.EActivity;

/**
 * 传感器
 */
@EActivity(R.layout.activity_senson)
public class SensonActivity extends BaseActivity implements ShakeListener.OnShakeListener {
    private ShakeListener shakeListener;
    private Vibrator vibrator;

    @Override
    public void initView() {
        shakeListener = new ShakeListener(this);
        shakeListener.setOnShakeListener(this);
        shakeListener.start();
        //震动
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        shakeListener.stop();
    }

    @Override
    public void onShake() {
        //根据自己的业务需求
        long[] pattern = {100, 400, 100, 400};   // 停止 开启 停止 开启
        vibrator.vibrate(pattern, 1);
        BaseApplication.getInstance().showToast("摇什么摇,又不会有对象");
    }
}
