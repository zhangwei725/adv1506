package com.qianfeng.zhangwei.extras.utils;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.extras.utils
 * @类 名： AppConfig
 * @创 建人： zhangwei
 * @创建时间：2015-07-05 19:24
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class AppConfig {
    /**
     * android EventBus tag
     */
    public static final String EVENT_BUS_TAG = "tag";
    public static final String ASYNC_TAG = "async";
    public static final String POST_TAG = "post";
    /**
     * app根目录
     */
    public static final String ROOT_PATH = "qianfeng";
    /**
     * 缓存图片保存路径
     */
    public static final String IMAGE_PATH = "image";


}
