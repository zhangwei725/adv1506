package com.qianfeng.zhangwei.extras.bean;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.bean
 * @类 名： Params
 * @创 建人： zhangwei
 * @创建时间：2015-07-06 17:29
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class Params {
    private int cp;
    private int count;
    private int page;
    private String sys;
    private int userId;
    private String imei;
    private int type;
    private int re;
    private String vid;
    private String uri;
    private String lang;
    private int requesttype;

    public void setCp(int cp) {
        this.cp = cp;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setSys(String sys) {
        this.sys = sys;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setRe(int re) {
        this.re = re;
    }

    public void setVid(String vid) {
        this.vid = vid;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public void setRequesttype(int requesttype) {
        this.requesttype = requesttype;
    }

    public int getCp() {
        return cp;
    }

    public int getCount() {
        return count;
    }

    public int getPage() {
        return page;
    }

    public String getSys() {
        return sys;
    }

    public int getUserId() {
        return userId;
    }

    public String getImei() {
        return imei;
    }

    public int getType() {
        return type;
    }

    public int getRe() {
        return re;
    }

    public String getVid() {
        return vid;
    }

    public String getUri() {
        return uri;
    }

    public String getLang() {
        return lang;
    }

    public int getRequesttype() {
        return requesttype;
    }
}
