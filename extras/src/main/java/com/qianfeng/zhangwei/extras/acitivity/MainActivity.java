package com.qianfeng.zhangwei.extras.acitivity;

import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.qianfeng.zhangwei.extras.BaseApplication;
import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.eventbus.EventBusActivity_;
import com.qianfeng.zhangwei.extras.meilishuo.BuyActivity_;
import com.qianfeng.zhangwei.extras.recycler.RecyclerActivity_;
import com.qianfeng.zhangwei.extras.sensor.SensonActivity_;
import com.qianfeng.zhangwei.extras.utils.AppManager;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ItemClick;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;


@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    public static final String URLTHREADGET = "http://www.ut2o.com/annualleave/home.json";
    public static final String URL_HOME = "http://www.ut2o.com/annualleave/exchange/home.json";
    @ViewById(R.id.main_listview)
    ListView listView;
    private ArrayList<String> list = new ArrayList<>();
//    @ViewById(R.id.main_error_vs)
//    ViewStub viewStub;
//    View errorView;


    @Override
    public void inject() {
        list.add("线程通信框架");
        list.add("环信");
        list.add("RecyclerView");
        list.add("美丽说+无限循环滚动");
        list.add("验证框架");
        list.add("联网工具类");
        list.add("传感器");


    }

    @Override
    public void initView() {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, list);
        listView.setAdapter(arrayAdapter);
//        在当前的可视界面刷新数据
//        arrayAdapter.notifyDataSetChanged();
//        刷新所有数据 跳回第一条数据
//        arrayAdapter.notifyDataSetInvalidated();


    }

//
//    private void showNormal() {
//        if (errorView != null) {
//            errorView.setVisibility(View.GONE);
//        }
//    }


    @Override
    public void initData() {

    }

    @ItemClick(R.id.main_listview)
    public void onItemClick(int position) {
        switch (position) {
            case 0:
                startActivity(new Intent(this, EventBusActivity_.class));
                break;
            case 1:
                //启动第三方的app
                Intent intent = getPackageManager().getLaunchIntentForPackage("com.qianfeng.zhangwei.easemob");
                if (intent != null) {
                    startActivity(intent);
                } else {
//                    提示下载
                }
                break;
            case 2:
                startActivity(new Intent(this, RecyclerActivity_.class));
                break;
            case 3:
                startActivity(new Intent(this, BuyActivity_.class));
                break;
            case 4:
                startActivity(new Intent(this, VerificationActivity_.class));
                break;
            case 5:
                BaseApplication.getInstance().showToast("查看BaseApplication类");
                break;
            case 6:
                startActivity(new Intent(this, SensonActivity_.class));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        BaseApplication.getInstance().unRegisterNetworkStateReceiver();
        AppManager.getAppManager().AppExit(BaseApplication.getInstance());
    }


}
