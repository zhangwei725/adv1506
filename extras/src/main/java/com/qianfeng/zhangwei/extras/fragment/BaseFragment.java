package com.qianfeng.zhangwei.extras.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.simple.eventbus.EventBus;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.extras.fragment
 * @类 名： BaseFragment
 * @创 建人： zhangwei
 * @创建时间：2015-07-05 20:30
 * @版 本：v1.0
 * @修 改人： activity ->> fragmentActiviyt- actionbaracitivty ---AppCompatActivity
 * @修改时间：
 * @修改备注：
 */
@EFragment
public class BaseFragment extends Fragment {
    public EventBus eventBus = EventBus.getDefault();
    public LayoutInflater inflater;
    public FragmentActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = (FragmentActivity) activity;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @AfterViews
    public void init() {
        initView();
        initData();
    }

    @AfterInject
    public void inject() {
        eventBus.register(this);

    }

    public void initView() {

    }

    public void initData() {

    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this);
    }
}
