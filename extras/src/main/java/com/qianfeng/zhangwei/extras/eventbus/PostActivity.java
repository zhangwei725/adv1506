package com.qianfeng.zhangwei.extras.eventbus;

import android.view.View;
import android.widget.Button;

import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.acitivity.BaseActivity;
import com.qianfeng.zhangwei.extras.utils.AppConfig;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.simple.eventbus.EventBus;

/**
 */
@EActivity(R.layout.activity_post)
public class PostActivity extends BaseActivity {
    @ViewById(R.id.default_btn)
    Button defaultBtn;
    @ViewById(R.id.default_tag_btn)
    Button defaultWithTagBtn;
    @ViewById(R.id.post_tag_btn)
    Button postWithTagBtn;
    @ViewById(R.id.post_async_tag_btn)
    Button postAsyncTagBtn;
    EventBus eventBus = EventBus.getDefault();

    @Click({R.id.default_btn, R.id.default_tag_btn, R.id.post_tag_btn, R.id.post_async_tag_btn})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.default_btn:
                eventBus.post("花开花落不变寻梦之心");
                break;
            case R.id.default_tag_btn:
                eventBus.post("花开花落不变寻梦之心,虫死虫生待语话之时", AppConfig.POST_TAG);
                break;
            case R.id.post_async_tag_btn:
                eventBus.post("Async---->>>花开花落不变寻梦之心,虫死虫生待语话之时", AppConfig.ASYNC_TAG);
                break;

        }
    }


}
