package com.qianfeng.zhangwei.extras.recycler.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.recycler.OnItemClickLinstener;

import java.util.List;


/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.extras.recycler.adapter
 * @类 名： MyRecyclerAdapter
 * @创 建人： zhangwei
 * @创建时间：2015-07-06 11:33
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 *
 */
public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    private List<String> list;

    private OnItemClickLinstener onItemClickLinstener;


    public MyRecyclerAdapter(List<String> list) {
        this.list = list;
    }

    /**
     * if (convertView == null){
     *      convertView
     * }else{
     *     vh = convertView.gettag()
     * }
     *
     * vh.xxx.setxxx()
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_layout, parent, false);
        MyViewHolder vh = new MyViewHolder(itemView);
        return vh;


    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.title.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView title;
        private View itemView;


        public MyViewHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            title = (TextView) itemView.findViewById(R.id.item_recycler_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (onItemClickLinstener != null) {
//            onItemClickLinstener.onItemClickLinstener(itemView,this.getPosition());
                onItemClickLinstener.onItemClickLinstener(list.get(getPosition()));
            }
        }
    }

    public OnItemClickLinstener getOnItemClickLinstener() {
        return onItemClickLinstener;
    }

    public void setOnItemClickLinstener(OnItemClickLinstener onItemClickLinstener) {
        this.onItemClickLinstener = onItemClickLinstener;
    }
}
