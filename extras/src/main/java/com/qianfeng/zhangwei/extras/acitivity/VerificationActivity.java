package com.qianfeng.zhangwei.extras.acitivity;

import android.graphics.Color;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.verification.Rule;
import com.qianfeng.zhangwei.extras.verification.TextStyle;
import com.qianfeng.zhangwei.extras.verification.Validator;
import com.qianfeng.zhangwei.extras.verification.annotation.Checked;
import com.qianfeng.zhangwei.extras.verification.annotation.ConfirmPassword;
import com.qianfeng.zhangwei.extras.verification.annotation.Email;
import com.qianfeng.zhangwei.extras.verification.annotation.IpAddress;
import com.qianfeng.zhangwei.extras.verification.annotation.NumberRule;
import com.qianfeng.zhangwei.extras.verification.annotation.Password;
import com.qianfeng.zhangwei.extras.verification.annotation.Regex;
import com.qianfeng.zhangwei.extras.verification.annotation.Required;
import com.qianfeng.zhangwei.extras.verification.annotation.TextRule;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_verification)
public class VerificationActivity extends BaseActivity implements Validator.ValidationListener {
    @Password(message = "请输入密码", order = 1)
    @ViewById
    EditText a;
    @ConfirmPassword(messageResId = R.string.err, order = 2)
    @ViewById
    EditText b;

    @Email(empty = false, message = "邮箱格式错误", order = 3)
    @ViewById
    EditText c;

    @IpAddress(message = "IP格式错误", order = 4)
    @ViewById
    EditText d;

    @NumberRule(type = NumberRule.NumberType.INTEGER, lt = 1, gt = 1000000, message = "输入数字错误", order = 5)
    @ViewById
    EditText e;
    @ViewById
    @Regex(message = "输入内容错误", trim = true, pattern = "[a-zA-Z0-9_]{6,15}", order = 6)
    EditText f;

    @ViewById

    @Required(message = "不能为空", order = 7)
    EditText g;

    @ViewById
    @TextRule(maxLength = 4, minLength = 2, message = "文字长度错误", order = 8)
    EditText h;

    @ViewById

    @Checked(checked = true, message = "必须选中服务条款", order = 9)
    CheckBox checkbox1;

    Validator validator;

    @Click(R.id.judge)
    public void click() {
        //这里是做验证
        validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        Toast.makeText(this, "验证通过", Toast.LENGTH_SHORT).show();
    }

    /**
     * 提示的形式可以自己定义
     */
    @Override
    public void onValidationFailed(View failedView, Rule<?> failedRule) {
        String message = failedRule.getFailureMessage();
        if (failedView instanceof EditText) {
            failedView.requestFocus();
            TextStyle textStyle = new TextStyle();
            textStyle.setString(message);
            textStyle.setBackgroundColor(Color.RED, 0, message.length());
            ((EditText) failedView).setError(textStyle.getSpannableString());
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

}
