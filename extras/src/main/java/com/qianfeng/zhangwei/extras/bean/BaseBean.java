package com.qianfeng.zhangwei.extras.bean;

import java.io.Serializable;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.bean
 * @类 名： BaseBean
 * @创 建人： zhangwei
 * @创建时间：2015-07-14 11:35
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class BaseBean implements Serializable {
}
