package com.qianfeng.zhangwei.extras.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.utils
 * @类 名： ImageUtils
 * @创 建人： zhangwei
 * @创建时间：2015-07-15 16:16
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class ImageUtils {
    /***
     *
     * @param imagePath
     * @return
     */
    private Bitmap onSampSize(String imagePath, int maxHeight, int maxWidth) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        //图片不要加载进内存
        options.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(imagePath,options);


        //获得图片的实际大小
        int outHeight = options.outHeight;
        int outWidth = options.outWidth;


        int sampleSize = 1; //默认缩放为1


        while (outHeight / sampleSize > maxHeight || outWidth / sampleSize > maxWidth) {
            sampleSize *= 2;
        }
        options.inSampleSize = sampleSize;
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imagePath, options);

    }


}
