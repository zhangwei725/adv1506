package com.qianfeng.zhangwei.extras.bean;

import java.io.Serializable;
import java.util.HashMap;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.bean
 * @类 名： Tititle
 * @创 建人： zhangwei
 * @创建时间：2015-07-08 11:56
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class Tititle  implements Serializable{
    private HashMap<String, HashMap<String, String>> hashMap;
    private String find;

    public HashMap<String, HashMap<String, String>> getHashMap() {
        return hashMap;
    }

    public void setHashMap(HashMap<String, HashMap<String, String>> hashMap) {
        this.hashMap = hashMap;
    }

    public String getFind() {
        return find;
    }

    public void setFind(String find) {
        this.find = find;
    }
}
