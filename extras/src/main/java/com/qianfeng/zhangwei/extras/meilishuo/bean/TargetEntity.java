package com.qianfeng.zhangwei.extras.meilishuo.bean;

import java.io.Serializable;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.bean
 * @类 名： TargetEntity
 * @创 建人： zhangwei
 * @创建时间：2015-07-07 22:27
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */

public class TargetEntity implements Serializable {
    private int id;
    private String title;
    private int updated_at;
    private int status;
    private int posts_count;
    private String subtitle;
    private int created_at;
    private String banner_image_url;
    private String cover_image_url;

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setUpdated_at(int updated_at) {
        this.updated_at = updated_at;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setPosts_count(int posts_count) {
        this.posts_count = posts_count;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public void setCreated_at(int created_at) {
        this.created_at = created_at;
    }

    public void setBanner_image_url(String banner_image_url) {
        this.banner_image_url = banner_image_url;
    }

    public void setCover_image_url(String cover_image_url) {
        this.cover_image_url = cover_image_url;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public int getUpdated_at() {
        return updated_at;
    }

    public int getStatus() {
        return status;
    }

    public int getPosts_count() {
        return posts_count;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public int getCreated_at() {
        return created_at;
    }

    public String getBanner_image_url() {
        return banner_image_url;
    }

    public String getCover_image_url() {
        return cover_image_url;
    }
}