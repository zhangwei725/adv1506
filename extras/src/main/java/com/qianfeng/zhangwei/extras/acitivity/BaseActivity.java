package com.qianfeng.zhangwei.extras.acitivity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;

import com.qianfeng.zhangwei.extras.utils.AppManager;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

@EActivity
public class BaseActivity extends AppCompatActivity {

    public LayoutInflater inflater;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppManager.getAppManager().addActivity(this);
        getSupportFragmentManager();
    }

    @AfterInject
    public void inject() {

    }

    @AfterViews
    public void init() {
        initView();
        initData();
    }

    public void initView() {

    }

    public void initData() {

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        AppManager.getAppManager().finishActivity(this);
    }
}
