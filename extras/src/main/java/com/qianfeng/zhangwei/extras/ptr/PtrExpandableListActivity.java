package com.qianfeng.zhangwei.extras.ptr;

import android.widget.ExpandableListView;

import com.handmark.pulltorefresh.library.PullToRefreshExpandableListView;
import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.acitivity.BaseActivity;
import com.qianfeng.zhangwei.extras.ptr.adapter.PtrExpandAdapter;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_ptr_expandable_list)
public class PtrExpandableListActivity extends BaseActivity {
    @ViewById
    PullToRefreshExpandableListView ptrListView;

    @Override
    public void initView() {
        ExpandableListView expandableListView = ptrListView.getRefreshableView();
        expandableListView.setAdapter(new PtrExpandAdapter());
    }

    @Override
    public void initData() {

    }
}
