package com.qianfeng.zhangwei.extras.meilishuo.adapter;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.lidroid.xutils.BitmapUtils;
import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.meilishuo.auto.RecyclingPagerAdapter;
import com.qianfeng.zhangwei.extras.meilishuo.bean.Banner;

import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.adapter
 * @类 名： AutoImagePageAdapter
 * @创 建人： zhangwei
 * @创建时间：2015-07-08 14:35
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class AutoImagePageAdapter extends RecyclingPagerAdapter implements ViewPager.OnPageChangeListener {
    private List<Banner> imgUrls;
    private Context context;
    private LayoutInflater inflater;
    private BitmapUtils bitmapUtils;
    private LinearLayout dotLayout;


    public AutoImagePageAdapter(List<Banner> imgUrls, Context context, LinearLayout dotLayout, BitmapUtils bitmapUtils) {
        this.imgUrls = imgUrls;
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.bitmapUtils = bitmapUtils;
        this.dotLayout = dotLayout;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup container) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_auto_layout, container, false);
            vh = new ViewHolder(convertView);
            convertView.setTag(vh);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        bitmapUtils.display(vh.img, imgUrls.get(getCurrentPosition(position)).getImage_url());

        return convertView;
    }

    private int getCurrentPosition(int position) {
        return position % imgUrls.size();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < imgUrls.size(); i++) {
            ImageView img = (ImageView) dotLayout.getChildAt(i);
            if (getCurrentPosition(position) == i) {
                img.setImageResource(R.mipmap.dot_white);
            } else {
                img.setImageResource(R.mipmap.dot_black);
            }

        }


    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class ViewHolder {
        ImageView img;

        public ViewHolder(View itemView) {
            this.img = (ImageView) itemView.findViewById(R.id.item_auto_img);
        }
    }


    @Override
    public int getCount() {
        return imgUrls != null && !imgUrls.isEmpty() ? Integer.MAX_VALUE : 0;
    }

    public void notifyDataSetChanged(List<Banner> list) {
        imgUrls.addAll(list);
        addDot();
        notifyDataSetChanged();
    }

    /**
     * 添加小圆点
     */
    private void addDot() {
        for (int i = 0; i < imgUrls.size(); i++) {
            ImageView img = new ImageView(context);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.rightMargin= 10;
            if (i == 0) {
                img.setImageResource(R.mipmap.dot_white);
            } else {
                img.setImageResource(R.mipmap.dot_black);
            }
            img.setLayoutParams(params);
            dotLayout.addView(img);
        }
    }


}
