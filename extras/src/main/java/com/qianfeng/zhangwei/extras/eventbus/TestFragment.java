package com.qianfeng.zhangwei.extras.eventbus;

import com.qianfeng.zhangwei.extras.BaseApplication;
import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.fragment.BaseFragment;
import com.qianfeng.zhangwei.extras.utils.AppConfig;

import org.androidannotations.annotations.EFragment;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

/**
 * 第一步实例化对象
 *
 * 第二步注册  注销
 *
 * 第三步 声明接受的函数 添加注解@Subscribe
 */
@EFragment(R.layout.fragment_test)
public class TestFragment extends BaseFragment {
    /**
     * @param str
     * @Subscriber 说明该方法是接受消息的方法
     */
    @Subscriber
    public void testEventBus(String str) {
        BaseApplication.getInstance().showToast(str);
    }


    @Subscriber(tag = AppConfig.POST_TAG)
    public void testMsgWithTag(String str) {
        BaseApplication.getInstance().showToast(str);
    }


    @Subscriber(tag = AppConfig.ASYNC_TAG,mode = ThreadMode.ASYNC)
    public void testMsgWithAsync(String str) {
        BaseApplication.getInstance().showToast(str);
    }

}
