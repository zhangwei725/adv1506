package com.qianfeng.zhangwei.extras.meilishuo.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lidroid.xutils.BitmapUtils;
import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.adapter.AppBaseAdapter;
import com.qianfeng.zhangwei.extras.meilishuo.bean.Shop;

import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.adapter
 * @类 名： PtrStaggerAdapter
 * @创 建人： zhangwei
 * @创建时间：2015-07-07 22:04
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class PtrStaggerAdapter extends AppBaseAdapter<Shop> {
    private BitmapUtils bitmapUtils;

    public PtrStaggerAdapter(List<Shop> list, Context context, BitmapUtils bitmapUtils) {
        super(list, context);
        this.bitmapUtils = bitmapUtils;
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        ViewHolder vh;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_ptrsgv_layout, parent, false);
            vh = new ViewHolder(convertView);
        } else {
            vh = (ViewHolder) convertView.getTag();
        }
        bitmapUtils.configDefaultBitmapMaxSize(Integer.valueOf(list.get(position).getPic_width()), Integer.valueOf(list.get(position).getPic_height()));
        bitmapUtils.display(vh.img, list.get(position).getPic_url());
        vh.content.setText(list.get(position).getRemark());
        return convertView;
    }

    private static class ViewHolder {
        private ImageView img;
        private TextView content;
        public ViewHolder(View itemView) {
            this.img = (ImageView) itemView.findViewById(R.id.item_ptrsgv_img);
            content = (TextView) itemView.findViewById(R.id.item_ptrsgv_content);
            itemView.setTag(this);
        }
    }

}
