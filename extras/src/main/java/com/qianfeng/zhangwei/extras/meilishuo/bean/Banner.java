package com.qianfeng.zhangwei.extras.meilishuo.bean;

import java.io.Serializable;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.bean
 * @类 名： Banner
 * @创 建人： zhangwei
 * @创建时间：2015-07-07 22:09
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class Banner implements Serializable {

    private int id;
    private int order;
    private int status;
    private String image_url;
    private TargetEntity target;
    private String type;
    private String target_url;
    private int target_id;

    public void setId(int id) {
        this.id = id;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void setTarget(TargetEntity target) {
        this.target = target;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTarget_url(String target_url) {
        this.target_url = target_url;
    }

    public void setTarget_id(int target_id) {
        this.target_id = target_id;
    }

    public int getId() {
        return id;
    }

    public int getOrder() {
        return order;
    }

    public int getStatus() {
        return status;
    }

    public String getImage_url() {
        return image_url;
    }

    public TargetEntity getTarget() {
        return target;
    }

    public String getType() {
        return type;
    }

    public String getTarget_url() {
        return target_url;
    }

    public int getTarget_id() {
        return target_id;
    }

}
