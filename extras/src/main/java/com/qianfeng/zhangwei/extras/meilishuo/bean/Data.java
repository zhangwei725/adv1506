package com.qianfeng.zhangwei.extras.meilishuo.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.bean
 * @类 名： Data
 * @创 建人： zhangwei
 * @创建时间：2015-07-08 15:18
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class Data implements Serializable {

    private List<Banner> banners;


    public List<Banner> getBanners() {
        return banners;
    }

    public void setBanners(List<Banner> banners) {
        this.banners = banners;
    }
}
