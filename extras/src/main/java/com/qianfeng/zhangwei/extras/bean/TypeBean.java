package com.qianfeng.zhangwei.extras.bean;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Table;

import java.io.Serializable;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.bean
 * @类 名： TypeBean
 * @创 建人： zhangwei
 * @创建时间：2015-07-06 20:45
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */

@Table(name = "TB_TYPE")
public class TypeBean implements Serializable {
    @Column(column = "type")
    private String type;
    @Column(column = "name")
    private String name;
    @Column(column = "image")
    private String image;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
