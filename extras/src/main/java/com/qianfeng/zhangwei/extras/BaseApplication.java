package com.qianfeng.zhangwei.extras;

import android.app.Application;
import android.widget.Toast;

import com.lidroid.xutils.bitmap.BitmapGlobalConfig;
import com.lidroid.xutils.cache.MD5FileNameGenerator;
import com.qianfeng.zhangwei.extras.netstate.NetChangeObserver;
import com.qianfeng.zhangwei.extras.netstate.NetworkStateReceiver;
import com.qianfeng.zhangwei.extras.utils.AppConfig;
import com.qianfeng.zhangwei.extras.utils.NetChangeObserverUtils;

import java.io.File;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：BaseApplication
 * @创 建人：zhangwei
 * @创建时间：2015-07-04 17 : 53
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class BaseApplication extends Application {
    private static BaseApplication application;
    private NetChangeObserver observer = new NetChangeObserverUtils();

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
        //初始化环信Sdk
        bitmapGlobalConfig();
        registerNetwork();

    }

    /**
     * 注册网络变化监听类
     */
    private void registerNetwork() {
        NetworkStateReceiver.registerNetworkStateReceiver(this);
        NetworkStateReceiver.registerObserver(observer);
    }


    /**
     * 图片加载框架全局配置
     *
     * approot/iamgecache
     */
    private void bitmapGlobalConfig() {
        BitmapGlobalConfig config = BitmapGlobalConfig.getInstance(this, AppConfig.ROOT_PATH + File.separator + AppConfig.IMAGE_PATH);
        config.setFileNameGenerator(new MD5FileNameGenerator());
        config.setThreadPoolSize(4);
    }

    /**
     *
     */
    public void unRegisterNetworkStateReceiver() {
        if (observer != null) {
            NetworkStateReceiver.removeRegisterObserver(observer);
        }
        NetworkStateReceiver.unRegisterNetworkStateReceiver(this);
    }


    public static BaseApplication getInstance() {
        return application;
    }


    public void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

}
