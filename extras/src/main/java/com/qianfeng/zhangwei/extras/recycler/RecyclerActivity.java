package com.qianfeng.zhangwei.extras.recycler;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.qianfeng.zhangwei.extras.BaseApplication;
import com.qianfeng.zhangwei.extras.R;
import com.qianfeng.zhangwei.extras.acitivity.BaseActivity;
import com.qianfeng.zhangwei.extras.recycler.adapter.MyRecyclerAdapter;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * ListView  gridview  画廊  瀑布流
 */
@EActivity(R.layout.activity_recycler)
public class RecyclerActivity extends BaseActivity {
    @ViewById(R.id.recyclerView)
    RecyclerView recyclerView;
    private ArrayList<String> list = new ArrayList<>();

    @Override
    public void inject() {
        for (int i = 0; i < 40; i++) {
            list.add("测试Recycler----------------" + i);
        }
    }

    @Override
    public void initView() {
        linearlayoutManger();
    }

    private void linearlayoutManger() {
        /**
         * 系统提供了三种类型的manager
         *LinearLayoutManager  线性布局管理器
         */

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        /**
         * recycler添加管理器
         */
        recyclerView.setLayoutManager(linearLayoutManager);
        /**
         *关联adapter
         */
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(list);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickLinstener(new OnItemClickLinstener() {
            @Override
            public void onItemClickLinstener(View itemView, int postion) {

            }

            @Override
            public <T> void onItemClickLinstener(T t) {
                BaseApplication.getInstance().showToast(t.toString());
            }
        });
    }


    /**
     * 网格管理器
     */
    private void girdLayoutManager() {
        /**
         * 参数说明
         * 1 上下文
         * 2 显示的列数
         * 3 滑动的方向 水平还是垂直
         * 4 true 是否从最后一行开始滑动
         *
         *
         *
         */
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        /**
         * recycler添加管理器
         */
        recyclerView.setLayoutManager(gridLayoutManager);
        /**
         *关联adapter
         */
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(list);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickLinstener(new OnItemClickLinstener() {
            @Override
            public void onItemClickLinstener(View itemView, int postion) {

            }

            @Override
            public <T> void onItemClickLinstener(T t) {
                BaseApplication.getInstance().showToast(t.toString());
            }
        });
    }

    /**
     * 瀑布流管理器
     */
    private void StaggerLayoutManager() {
        /**
         *
         */
        StaggeredGridLayoutManager staggerManager = new StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        /**
         * recycler添加管理器

         */
        recyclerView.setLayoutManager(staggerManager);
        /**
         *关联adapter
         */
        MyRecyclerAdapter adapter = new MyRecyclerAdapter(list);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickLinstener(new OnItemClickLinstener() {
            @Override
            public void onItemClickLinstener(View itemView, int postion) {

            }

            @Override
            public <T> void onItemClickLinstener(T t) {
                BaseApplication.getInstance().showToast(t.toString());
            }
        });
    }


    @Override
    public void initData() {
    }
}
