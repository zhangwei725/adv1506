package com.qianfeng.zhangwei.extras.meilishuo.utils;

import android.widget.BaseAdapter;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.HttpHandler;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.qianfeng.zhangwei.extras.meilishuo.bean.AppBean;

import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.utils
 * @类 名： HttpTools
 * @创 建人： zhangwei
 * @创建时间：2015-07-07 22:51
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class HttpTools {
    private static HttpTools tools;
    private HttpTools() {
    }


    public static HttpTools getInstance() {
        if (tools == null) {
            tools = new HttpTools();
        }
        return tools;
    }


    public <T> HttpHandler<String> getDataFromServer(String url, final BaseAdapter adapter, final List<T> list, final PullToRefreshBase pullToRefreshBase, final boolean isClear, final TypeReference<AppBean<T>> type) {
        HttpHandler<String> httpHandler = new HttpUtils().send(HttpRequest.HttpMethod.GET, url, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                AppBean<T> app = JSONObject.parseObject(responseInfo.result, type);
                if (app != null && app.getData() != null && !app.getData().isEmpty()) {
                    if (isClear) {
                        list.clear();
                    }
                    list.addAll(app.getData());
                    adapter.notifyDataSetChanged();
                }
                pullToRefreshBase.onRefreshComplete();
            }

            @Override
            public void onFailure(HttpException e, String s) {
                pullToRefreshBase.onRefreshComplete();
            }
        });
        return httpHandler;


    }



    public <T> HttpHandler<String> postDataFromServer(String url, RequestParams params, final BaseAdapter adapter, final List<T> list, final PullToRefreshBase pullToRefreshBase, final boolean isClear, final TypeReference<AppBean<T>> type) {
        HttpHandler<String> httpHandler = new HttpUtils().send(HttpRequest.HttpMethod.GET, url, params, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                AppBean<T> app = JSONObject.parseObject(responseInfo.result, type);
                if (app != null && app.getData() != null && !app.getData().isEmpty()) {
                    if (isClear) {
                        list.clear();
                    }
                    list.addAll(app.getData());
                    adapter.notifyDataSetChanged();
                }
                pullToRefreshBase.onRefreshComplete();
            }

            @Override
            public void onFailure(HttpException e, String s) {
                pullToRefreshBase.onRefreshComplete();
            }
        });

        return httpHandler;

    }


}
