package com.qianfeng.zhangwei.extras.utils;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.utils
 * @类 名： UrlConstants
 * @创 建人： zhangwei
 * @创建时间：2015-07-07 22:15
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class UrlConstants {
    /**
     * 瀑布流数据
     */
    public static final String MEILISHUO_SELECT_GET_URL = "http://mobapi.meilishuo.com/2.0/twitter/selected?r=navigation-left%3Apos%3D1%3Aname%3D%25E7%25B2%25BE%25E9%2580%2589&offset=0&limit=30&imei=000000000000000&mac=08%3A00%3A27%3A9d%3A00%3A06&qudaoid=10008&access_token=d2b3fb04fb95f3b7b617a99d659c66e0&device_id=mac_08%3A00%3A27%3A9d%3A00%3A06&st=1435712726&_sign=b15fc85c64153aa9ab77a14d600e6ac7e4294777";
    public static final String MEILISHUO_SELECT_POST_URL = "http://mobapi.meilishuo.com/2.0/twitter/selected";

    public static final String LIWUSHUO_BANNERS_URL = "http://api.liwushuo.com/v2/banners";

}
