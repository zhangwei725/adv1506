package com.qianfeng.zhangwei.extras.meilishuo.bean;

import java.io.Serializable;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.bean
 * @类 名： Gift
 * @创 建人： zhangwei
 * @创建时间：2015-07-07 22:29
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class Shop implements Serializable {

    private String service_id;
    private String pic_height;
    private String q_pic_url;
    private String o_pic_url;
    private String m_pic_url;
    private String remark;
    private String z_pic_url;
    private String service_type;
    private String j_pic_url;
    private String pic_url;
    private String title;
    private String pic_width;
    private String is_doota;
    private String shop_id;
    private int is_promote;
    private String tags_num;
    private String twitter_id;
    private String count_like;
    private String r;
    private String d_pic_url;
    private String alert_key;
    private String twitter_goods_id;
    private String d_r;

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public void setPic_height(String pic_height) {
        this.pic_height = pic_height;
    }

    public void setQ_pic_url(String q_pic_url) {
        this.q_pic_url = q_pic_url;
    }

    public void setO_pic_url(String o_pic_url) {
        this.o_pic_url = o_pic_url;
    }

    public void setM_pic_url(String m_pic_url) {
        this.m_pic_url = m_pic_url;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public void setZ_pic_url(String z_pic_url) {
        this.z_pic_url = z_pic_url;
    }

    public void setService_type(String service_type) {
        this.service_type = service_type;
    }

    public void setJ_pic_url(String j_pic_url) {
        this.j_pic_url = j_pic_url;
    }

    public void setPic_url(String pic_url) {
        this.pic_url = pic_url;
    }



    public void setTitle(String title) {
        this.title = title;
    }

    public void setPic_width(String pic_width) {
        this.pic_width = pic_width;
    }

    public void setIs_doota(String is_doota) {
        this.is_doota = is_doota;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public void setIs_promote(int is_promote) {
        this.is_promote = is_promote;
    }

    public void setTags_num(String tags_num) {
        this.tags_num = tags_num;
    }

    public void setTwitter_id(String twitter_id) {
        this.twitter_id = twitter_id;
    }

    public void setCount_like(String count_like) {
        this.count_like = count_like;
    }

    public void setR(String r) {
        this.r = r;
    }

    public void setD_pic_url(String d_pic_url) {
        this.d_pic_url = d_pic_url;
    }

    public void setAlert_key(String alert_key) {
        this.alert_key = alert_key;
    }

    public void setTwitter_goods_id(String twitter_goods_id) {
        this.twitter_goods_id = twitter_goods_id;
    }

    public void setD_r(String d_r) {
        this.d_r = d_r;
    }

    public String getService_id() {
        return service_id;
    }

    public String getPic_height() {
        return pic_height;
    }

    public String getQ_pic_url() {
        return q_pic_url;
    }

    public String getO_pic_url() {
        return o_pic_url;
    }

    public String getM_pic_url() {
        return m_pic_url;
    }

    public String getRemark() {
        return remark;
    }

    public String getZ_pic_url() {
        return z_pic_url;
    }

    public String getService_type() {
        return service_type;
    }

    public String getJ_pic_url() {
        return j_pic_url;
    }

    public String getPic_url() {
        return pic_url;
    }


    public String getTitle() {
        return title;
    }

    public String getPic_width() {
        return pic_width;
    }

    public String getIs_doota() {
        return is_doota;
    }

    public String getShop_id() {
        return shop_id;
    }

    public int getIs_promote() {
        return is_promote;
    }


    public String getTags_num() {
        return tags_num;
    }

    public String getTwitter_id() {
        return twitter_id;
    }

    public String getCount_like() {
        return count_like;
    }

    public String getR() {
        return r;
    }

    public String getD_pic_url() {
        return d_pic_url;
    }

    public String getAlert_key() {
        return alert_key;
    }

    public String getTwitter_goods_id() {
        return twitter_goods_id;
    }

    public String getD_r() {
        return d_r;
    }
}
