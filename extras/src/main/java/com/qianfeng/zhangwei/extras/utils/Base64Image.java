package com.qianfeng.zhangwei.extras.utils;

import android.util.Base64;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 *
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.utils
 * @类 名： Base64Image
 * @创 建人： zhangwei
 * @创建时间：2015-07-14 22:25
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class Base64Image {

    /**
     *  * @Descriptionmap 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
     *  * @param path 图片路径
     *  * @return
     *  
     */
    public static String imageToBase64(String path) {// 将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        byte[] data = null;
        // 读取图片字节数组
        try {
            InputStream in = new FileInputStream(path);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 对字节数组Base64编码

        return String.valueOf(Base64.encode(data, Base64.DEFAULT));// 返回Base64编码过的字节数组字符串
    }

    /**
     *  * @Descriptionmap 对字节数组字符串进行Base64解码并生成图片
     *  * @param base64 图片Base64数据
     *  * @param path 图片路径
     *  * @return
     *  
     */
    public static boolean base64ToImage(String base64, String path) {// 对字节数组字符串进行Base64解码并生成图片
        if (base64 == null) { // 图像数据为空
            return false;
        }
        try {
            // Base64解码
            byte[] bytes = Base64.decode(base64, Base64.DEFAULT);
            for (int i = 0; i < bytes.length; ++i) {
                if (bytes[i] < 0) {// 调整异常数据
                    bytes[i] += 256;
                }
            }
            // 生成jpeg图片
            OutputStream out = new FileOutputStream(path);
            out.write(bytes);
            out.flush();
            out.close();
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
