package com.qianfeng.zhangwei.extras.meilishuo.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @说 明：
 * @项目名称：1506adv
 * @包 名： com.qianfeng.zhangwei.extras.meilishuo.bean
 * @类 名： AppBean
 * @创 建人： zhangwei
 * @创建时间：2015-07-07 22:58
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class AppBean<T> implements Serializable {

    private  String r;

    private List<T> data;


    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
