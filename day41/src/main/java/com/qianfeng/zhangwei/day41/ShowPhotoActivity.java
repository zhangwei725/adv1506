package com.qianfeng.zhangwei.day41;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;
import com.lidroid.xutils.view.annotation.event.OnItemClick;
import com.qianfeng.zhangwei.day41.adapter.ImageShowAdapter;
import com.qianfeng.zhangwei.day41.bean.UpdateImage;
import com.qianfeng.zhangwei.day41.utils.FileUtils;

import java.io.File;
import java.io.RandomAccessFile;
import java.util.ArrayList;


/**
 * form
 *
 * http://10.0.164.239:8080/app/uplaodFile/upload
 * 拍照完成 -->显示图片-->选择图片-->上传到服务器器
 */
@ContentView(R.layout.activity_showphoto)
public class ShowPhotoActivity extends AppCompatActivity {
    @ViewInject(R.id.gridview)
    private GridView gridView;
    private ArrayList<UpdateImage> bitmaps = new ArrayList<>();
    private ImageShowAdapter adapter;
    public static final String UPDTAE_URL = "http://10.0.164.239:8080/app/uplaodFile/upload";
    @ViewInject(R.id.update)
    private Button updateBtn;


    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        initView();
        initData();
    }

    private void update() {
        HttpUtils httpUtils = new HttpUtils();
        RequestParams params = new RequestParams();
        for (int i = 0; i < bitmaps.size(); i++) {
            if (bitmaps.get(i).isSelect()) {
                params.addBodyParameter("" + i, bitmaps.get(i).getImageFile());
            }
        }
//        String iamgePath = bitmaps.get(0).getImageFile().getAbsolutePath();
//        String imageName = iamgePath.substring(iamgePath.lastIndexOf("/") + 1);

        httpUtils.send(HttpRequest.HttpMethod.POST, UPDTAE_URL, params, new RequestCallBack<String>() {

            @Override
            public void onLoading(long total, long current, boolean isUploading) {
                super.onLoading(total, current, isUploading);
                if (isUploading){
                    updateBtn.setText(current/total + "文件上传中");
                }
            }

            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                BaseApp.getInstance().showToast(responseInfo.result);
            }

            @Override
            public void onFailure(HttpException e, String s) {

            }
        });


    }


    /**
     * 初始化
     */
    private void initView() {
        adapter = new ImageShowAdapter(bitmaps, this);
        gridView.setAdapter(adapter);
    }

    private void initData() {
        File imageFile = new File(FileUtils.getPhotoPath());
        if (imageFile != null && imageFile.exists()) {
            File[] files = imageFile.listFiles();
            if (files != null && files.length > 0) {
                for (File file : files) {
                    Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                    if (bitmap != null && FileUtils.getBitmapSize(bitmap) > 0) {
                        UpdateImage updateImage = new UpdateImage(bitmap, false, file);
                        bitmaps.add(updateImage);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        }
    }
    //下载




    @OnItemClick(R.id.gridview)
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        ImageShowAdapter.ViewHolder holder = (ImageShowAdapter.ViewHolder) view.getTag();
        UpdateImage image = bitmaps.get(position);
        if (image.isSelect()) {
            image.setIsSelect(false);
            holder.cb.setChecked(false);
        } else {
            holder.cb.setChecked(true);
            image.setIsSelect(true);
        }

    }

    @OnClick(R.id.update)
    public void onCLickListener(View view) {
        update();
    }


}
