package com.qianfeng.zhangwei.day41.bean;

import android.graphics.Bitmap;

import java.io.File;

/**
 * @说 明：
 * @项目名称：1506adc
 * @包 名： com.qianfeng.zhangwei.day39
 * @类 名：UpdateImage
 * @创 建人：zhangwei
 * @创建时间：2015-06-30 14 : 12
 * @版 本：v1.0
 * @修 改人：
 * @修改时间：
 * @修改备注：
 */
public class UpdateImage {
    private Bitmap bitmap;
    private boolean isSelect;
    private File imageFile;


    public UpdateImage(Bitmap bitmap, boolean isSelect, File imageFile) {
        this.bitmap = bitmap;
        this.isSelect = isSelect;
        this.imageFile = imageFile;
    }

    public UpdateImage() {
    }

    public UpdateImage(Bitmap bitmap, boolean isSelect) {
        this.bitmap = bitmap;
        this.isSelect = isSelect;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setIsSelect(boolean isSelect) {
        this.isSelect = isSelect;
    }

    public File getImageFile() {
        return imageFile;
    }

    public void setImageFile(File imageFile) {
        this.imageFile = imageFile;
    }
}
