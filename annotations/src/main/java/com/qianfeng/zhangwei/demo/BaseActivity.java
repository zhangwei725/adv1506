package com.qianfeng.zhangwei.demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

public class BaseActivity extends AppCompatActivity {


    @AfterViews
    public void init() {
        initView();
        loadData();

    }


    public void initView() {

    }


    public void loadData() {

    }

}
