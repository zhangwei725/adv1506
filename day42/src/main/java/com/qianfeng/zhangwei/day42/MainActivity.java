package com.qianfeng.zhangwei.day42;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


/**
 * 第一步添加jar包
 * 第二步 asset目录下加入快捷支付的apk
 * 第三步 点击购买商品
 * 3.1>生成订单信息
 * 需求修改的信息  商品名称 商品介绍  商品价格
 * key文件下的  pid 2088
 * 私钥
 * 公钥
 * 支付账号
 *
 * 3.2> 给订单信息加密
 * 3.3> 订单信息转化成url
 * 3.4> 异步发起支付
 * 实例化alipay()   参数  上下文 参数,处理消息的handler
 * String result (返回第三方支付平台支付信息)=   alipay.pay(订单信息)
 * 3.5> 根据返回的信息执行相关的业务
 *
 * PoPupWindow
 */
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity {
    private PopupWindow popupWindow;
    @ViewById(R.id.title)
    Button title;


    @AfterViews
    public void init() {
        initPopUpWindow();
    }

    private void initPopUpWindow() {
        popupWindow = new PopupWindow(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        View view = getLayoutInflater().inflate(R.layout.pop_content_layout, null);
        ListView listView = (ListView) view.findViewById(R.id.pop_listview);
        listView.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, android.R.id.text1, new String[]{"测试", "测试", "测试", "测试", "测试"}));
        popupWindow.setContentView(view);
        //设置popwindow
        popupWindow.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.red)));
        popupWindow.setTouchInterceptor(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == KeyEvent.KEYCODE_BACK) {
                    return true;
                }
                return false;
            }
        });

    }

    @Click(R.id.title)
    public void onClick(View view) {
        if (popupWindow.isShowing()) {
            popupWindow.dismiss();
        } else {
            popupWindow.showAsDropDown(title);
            //xoff x轴上的偏移量  相对于显示的控件    yoff
//            popupWindow.showAsDropDown(title, 100, 100);

//            popupWindow.showAsDropDown(title,100,100, Gravity.CENTER);

//                popupWindow.showAtLocation(title, Gravity.CENTER,100,300);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus){
            popupWindow.showAsDropDown(title);
        }


    }
}
