package com.qianfeng.zhangwei.day39;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day39
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-26 09:29
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class MyLayout1 extends LinearLayout {
    private static final String TAG = "MyLayout1";

    public MyLayout1(Context context) {
        super(context);
    }

    public MyLayout1(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public MyLayout1(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 事件分发
     * 1.return false
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.e(TAG, "------------>>>>dispatchTouchEvent");
        return false;

    }

    /**
     * 事件拦截
     *
     * @param ev
     * @return
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.e(TAG, "------------>>>>onInterceptTouchEvent");
        return super.onInterceptTouchEvent(ev);
    }

    /**
     * 事件处理
     *
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                Log.e(TAG, "MotionEvent------>>ACTION_DOWN");
                break;
            case MotionEvent.ACTION_MOVE:
//                Log.e(TAG,"MotionEvent------>>ACTION_DOWN");
                break;
            case MotionEvent.ACTION_UP:
//                Log.e(TAG,"MotionEvent------>>ACTION_DOWN");
                break;

        }
        return super.onTouchEvent(event);
    }
}
