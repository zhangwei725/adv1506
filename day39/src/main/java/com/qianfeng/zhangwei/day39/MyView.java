package com.qianfeng.zhangwei.day39;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day39
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-26 09:28
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class MyView extends View {
    private static final String TAG = "MyView";

    public MyView(Context context) {
        super(context);
    }

    public MyView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        Log.e(TAG, "------------>>>>dispatchTouchEvent");
        return super.dispatchTouchEvent(event);
    }

    /**
     * 1 返回系统默认 false  以冒泡形式 从子元素依次向上传递事件
     * 2. true 事件不向上传递
     *
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.e(TAG, "onTouchEvent");
//        switch (event.getAction()) {
//            case MotionEvent.ACTION_DOWN:
//                //获得手指屏幕的坐标
//                event.getX();
//                //获得手指在屏幕上y坐标
//                event.getY();
//                //获得控件左上角在屏幕的x坐标
//                event.getRawX();
//                //获得控件左上角在屏幕的Y坐标
//                event.getRawX();
//            case MotionEvent.ACTION_MOVE:
//
//                break;
//            case MotionEvent.ACTION_UP:
//
//                break;
//
//        }
        return false;
    }

}
