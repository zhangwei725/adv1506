package com.qianfeng.zhangwei.day39.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qianfeng.zhangwei.day39.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SlidingFragment extends Fragment {


    public SlidingFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sliding, container, false);
    }


}
