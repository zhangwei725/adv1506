package com.qianfeng.zhangwei.day39;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.LinearLayout;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：
 * @包 名： com.qianfeng.zhangwei.day39
 * @类名称：
 * @创建人：张唯
 * @创建时间：2015-06-26 09:29
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class MyLayout extends LinearLayout {
    private static final String TAG = "MyLayout";

    public MyLayout(Context context) {
        super(context);
    }

    public MyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public MyLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * return true false 不想下分发
     *
     * @param ev
     * @return
     */
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.e(TAG, "------------>>>>dispatchTouchEvent");
        return super.dispatchTouchEvent(ev);
    }

    /**
     * return  true  拦截事件不向下分发事件,执行自己的事件处理
     * retur false  不拦截事件
     *
     * @param ev
     * @return 传递的 ActionDown事件
     */
    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        Log.e(TAG, "onInterceptTouchEvent");
        return super.onInterceptTouchEvent(ev);
    }

    /**
     * @param event
     * @return
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Log.e(TAG, "------------>>>>onTouchEvent");
        return super.onTouchEvent(event);
    }
}
