package com.qianfeng.zhangwei.day39.slidingmenu;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.qianfeng.zhangwei.day39.R;
import com.qianfeng.zhangwei.day39.fragment.SlidingFragment;

/**
 * 初始化SlidingMenu的三种方式
 *
 * 1>第一种  通过代码直接 new
 * 2>第二种方式  继承第三方库提供SlidingFragmentActivity  然后通过get方式去实例化
 * 3>在布局文件中声明
 */
public class SlidingMenuActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slidingmenu);
//        SlidingMenu menu = getSlidingMenu();
        initSlidingMenu();

    }

    /**
     * 如何使用 slidingMenu.setMode(SlidingMenu.LEFT_RIGHT);
     *
     * 必须设置左右两个布局  否则出异常
     *
     *
     *
     */

    private void initSlidingMenu() {
        SlidingMenu slidingMenu = new SlidingMenu(this);
        /**
         *  SlidingMenu.LEFT        从左边滑出侧滑菜单
         *  SlidingMenu.LEFT_RIGHT  左右两边都能滑出侧滑菜单
         *  SlidingMenu.RIGHT       从右边滑出侧滑菜单
         */

        slidingMenu.setMode(SlidingMenu.LEFT_RIGHT);

        /**
         * SlidingMenu.TOUCHMODE_FULLSCREEN 能通过全屏手势操作滑出侧滑菜单
         * SlidingMenu.TOUCHMODE_MARGIN     通过边缘滑出侧滑菜单
         * SlidingMenu.TOUCHMODE_NONE       不能通过手势滑出侧滑菜单
         */
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);

        DisplayMetrics displayMetrics = new DisplayMetrics();

        WindowManager windowManager = getWindowManager();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        //屏幕的宽度
        slidingMenu.setBehindOffset(displayMetrics.widthPixels / 2);
        /**
         *
         * SlidingMenu.SLIDING_WINDOW ActionBar跟着一起滑动
         *
         *SLIDING_CONTENT  ActionBar固定顶部,slidingmenu滑动
         */
        slidingMenu.setMenu(R.layout.sliding_left_layout);
//        slidingMenu.setSecondaryMenu();




        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);

        getSupportFragmentManager().beginTransaction().replace(R.id.slding_container, new SlidingFragment()).commit();



        slidingMenu.toggle();

        //显示侧滑菜单

//        slidingMenu.showMenu();

        //显示主Activity的内容
//        slidingMenu.showContent();


//        slidingMenu.setAboveOffsetRes();


//        View view  = null;
//        view.setVisibility(View.VISIBLE);


//        slidingMenu.setAboveOffsetRes(R.dimen.sliding_offset_width);


    }


}
