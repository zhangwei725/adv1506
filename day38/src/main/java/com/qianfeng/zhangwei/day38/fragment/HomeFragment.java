package com.qianfeng.zhangwei.day38.fragment;


import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.alibaba.fastjson.JSONObject;
import com.felipecsl.asymmetricgridview.library.Utils;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;
import com.lidroid.xutils.BitmapUtils;
import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.bitmap.PauseOnScrollListener;
import com.lidroid.xutils.db.sqlite.Selector;
import com.lidroid.xutils.db.sqlite.WhereBuilder;
import com.lidroid.xutils.exception.DbException;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.qianfeng.zhangwei.day38.BaseApp;
import com.qianfeng.zhangwei.day38.R;
import com.qianfeng.zhangwei.day38.adapter.AsyAdapter;
import com.qianfeng.zhangwei.day38.adapter.AsyMAdapter;
import com.qianfeng.zhangwei.day38.adapter.BannerAdapter;
import com.qianfeng.zhangwei.day38.adapter.ViewPagerAdapter;
import com.qianfeng.zhangwei.day38.bean.Banner;
import com.qianfeng.zhangwei.day38.bean.Home;
import com.qianfeng.zhangwei.day38.bean.MixProduct;
import com.qianfeng.zhangwei.day38.bean.Rotation;
import com.qianfeng.zhangwei.day38.config.UrlConstants;
import com.qianfeng.zhangwei.day38.widget.AutoScrollViewPager;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

/**
 * It 就是对数据库增删改查
 */
public class HomeFragment extends BaseFragment {
    private LayoutInflater inflater;
    BitmapUtils bitmapUtils;
    private HeadHolder holder;


    /**
     * 主listview
     */
    @ViewInject(R.id.home_listview)
    private ListView listView;
    private BannerAdapter bannerAdapter;
    private ArrayList<Banner> banners = new ArrayList<>();

    /**
     * 广告栏
     */
    private ArrayList<View> rotationViews = new ArrayList<>();
    private ViewPagerAdapter viewPagerAdapter;

    /**
     *
     *
     */


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initBitmUtils();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ViewUtils.inject(this, view);
        initView();
        loadData();
        return view;
    }

    /**
     * 初始化BitmapUtils工具类
     */
    private void initBitmUtils() {
        bitmapUtils = new BitmapUtils(getActivity());
        bitmapUtils.configDefaultLoadFailedImage(R.mipmap.ic_launcher);
        bitmapUtils.configDefaultLoadingImage(R.mipmap.ic_launcher);
    }

    /**
     * 初始化view
     */
    private void initView() {
        listView.setOnScrollListener(new PauseOnScrollListener(bitmapUtils, true, false));
        bannerAdapter = new BannerAdapter(getActivity(), banners, bitmapUtils);
        View view = createHeadView();
        listView.addHeaderView(view);
        listView.setAdapter(bannerAdapter);
    }

    private View createHeadView() {
        View headView = inflater.inflate(R.layout.home_head_layout, listView, false);
        holder = new HeadHolder(headView);
        viewPagerAdapter = new ViewPagerAdapter(rotationViews);
        holder.headPager.setAdapter(viewPagerAdapter);
        return headView;
    }


    private class HeadHolder {
        @ViewInject(R.id.home_head_viewpager)
        AutoScrollViewPager headPager;
        @ViewInject(R.id.home_head_dotll)
        LinearLayout dotLl;

        public HeadHolder(View view) {
            ViewUtils.inject(this, view);
        }
    }


    private void loadData() {
        HttpUtils httpUtils = new HttpUtils();
        //获取请求参数
        RequestParams params = getRquestParams();
        httpUtils.send(HttpRequest.HttpMethod.POST, UrlConstants.HOME_URL, params, new RequestCallBack<String>() {
            @Override
            public void onSuccess(ResponseInfo<String> responseInfo) {
                //获得服务器返回的json数据
                String result = responseInfo.result;
                Home home = JSONObject.parseObject(result, Home.class);
                if (home != null) {
                    if (home.getBanner() != null && home.getBanner().size() > 0) {
                        banners.addAll(home.getBanner());
                        bannerAdapter.notifyDataSetChanged();
                    }
                    if (home.getRotation() != null && !home.getRotation().isEmpty()) {
                        refreshRotation(home.getRotation());
                    }

                }
            }

            @Override
            public void onFailure(HttpException e, String s) {
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        holder.headPager.startAutoScroll();
    }


    @Override
    public void onPause() {
        super.onPause();
        holder.headPager.stopAutoScroll();
    }


    /**
     * 数据广告栏的
     */

    private void refreshRotation(List<Rotation> rotations) {
        for (int i = 0; i < rotations.size(); i++) {
            ImageView img = new ImageView(getActivity());
            LinearLayout.LayoutParams pararms = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            pararms.leftMargin = 10;
            img.setLayoutParams(pararms);
            bitmapUtils.display(img, rotations.get(i).getPhoto());
            rotationViews.add(img);
        }
        viewPagerAdapter.notifyDataSetChanged();


    }

    /**
     * 获取请求参数
     *
     * @return
     */
    private RequestParams getRquestParams() {
        RequestParams params = new RequestParams();
        params.addBodyParameter("sign", "f8257b7dd195415f4ad65e851844b762");
        params.addBodyParameter("timestamp", "1434703913");
        params.addBodyParameter("platform", "ANDROID");
        params.addBodyParameter("source", "app");
        params.addBodyParameter("connect_id", "");
        params.addBodyParameter("region_id", "143949");
        params.addBodyParameter("service", "marketing.banner");
        params.addBodyParameter("device_id", "a781ff387d0fd135e5a97ed41f71838c");
        params.addBodyParameter("channel", "qq");
        params.addBodyParameter("version", "2.1.1");
        return params;


    }


    private void db() {
        //                        DbUtils dbUtils = BaseApp.getInstance().getDbUtils();
//                        try {
//                            Banner banner = home.getBanner().get(0);
//                            //删除对象
//                            dbUtils.delete(banner);
//                            //保存多个对象
//                            dbUtils.saveAll(home.getBanner());
////                        ORM
////                        注解  IOC
////                        dbUtils.update();
//                            //分页查找
//                            List<Banner> list = dbUtils.findAll(Selector.from(Banner.class)
//                                    .limit(pageSize)
//                                    .offset(20));

//                        } catch (DbException e) {
//                            e.printStackTrace();
//                        }
    }


}
