package com.qianfeng.zhangwei.day38.adapter;

import android.content.Context;
import android.widget.ListAdapter;

import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;
import com.qianfeng.zhangwei.day38.bean.MixProduct;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.adapter
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 22:54
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class AsyMAdapter<MixProduct> extends AsymmetricGridViewAdapter {
    public AsyMAdapter(Context context, AsymmetricGridView listView, ListAdapter adapter) {
        super(context, listView, adapter);
    }
}
