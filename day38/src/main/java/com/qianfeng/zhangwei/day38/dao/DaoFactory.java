package com.qianfeng.zhangwei.day38.dao;

import com.qianfeng.zhangwei.day38.dao.impl.UserDaoImpl;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.dao
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 14:54
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class DaoFactory {
    private static UserDao userDao;

    /**
     * 获取操作user对象的Dao
     * @return
     */
    public static UserDao getUserDao() {
        if (userDao== null){
            userDao = new UserDaoImpl();
        }
        return userDao;
    }


}
