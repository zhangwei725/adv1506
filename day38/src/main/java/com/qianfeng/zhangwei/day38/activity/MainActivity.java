package com.qianfeng.zhangwei.day38.activity;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RadioGroup;

import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ContentView;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.qianfeng.zhangwei.day38.R;
import com.qianfeng.zhangwei.day38.fragment.CartFragment;
import com.qianfeng.zhangwei.day38.fragment.CategoryFragment;
import com.qianfeng.zhangwei.day38.fragment.GuoshiFragment;
import com.qianfeng.zhangwei.day38.fragment.HomeFragment;
import com.qianfeng.zhangwei.day38.fragment.SmileFragment;
import com.qianfeng.zhangwei.day38.utils.FragmentTabUtils;

import java.util.ArrayList;

/**
 *
 * 第一步  初始化Ui 布局文件定义
 * 通过代码  ----->>>  第一周第二周
 *
 * 第二步 加载数据 --数据来源->>  服务器  ---> httpUrlconcentration
 *                                        htppclient
 *                                        Thread + handler
 *                                        AsyncTask
 *                                        Volley
 *                                        httpUtils
 *                            本地   ----->>  sqlite  file
 *
 *  第三步 给控件赋值      text.settext();
 *
 */
@ContentView(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements FragmentTabUtils.OnRgsExtraCheckedChangedListener {
    @ViewInject(R.id.main_rgs)
    private RadioGroup rgs;
    ArrayList<Fragment> fragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewUtils.inject(this);
        fragments.add(new HomeFragment());
        fragments.add(new CategoryFragment());
        fragments.add(new SmileFragment());
        fragments.add(new CartFragment());
        fragments.add(new GuoshiFragment());
        new FragmentTabUtils(getSupportFragmentManager(), fragments, R.id.fragment_container, rgs).setOnRgsExtraCheckedChangedListener(this);
    }

    @Override
    public void OnRgsExtraCheckedChanged(RadioGroup radioGroup, int checkedId, int index) {

    }
}
