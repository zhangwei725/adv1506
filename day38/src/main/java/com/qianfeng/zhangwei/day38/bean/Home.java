package com.qianfeng.zhangwei.day38.bean;

import java.io.Serializable;
import java.util.List;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.bean
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 09:51
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class Home implements Serializable {

    private List<Banner> banner;

    private List<Rotation> rotation;


    private List<MixProduct> mix_product_banner;

    public List<MixProduct> getMix_product_banner() {
        return mix_product_banner;
    }

    public void setMix_product_banner(List<MixProduct> mix_product_banner) {
        this.mix_product_banner = mix_product_banner;
    }

    public List<Banner> getBanner() {
        return banner;
    }

    public void setBanner(List<Banner> banner) {
        this.banner = banner;
    }

    public List<Rotation> getRotation() {
        return rotation;
    }

    public void setRotation(List<Rotation> rotation) {
        this.rotation = rotation;
    }
}
