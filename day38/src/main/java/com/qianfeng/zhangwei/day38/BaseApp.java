package com.qianfeng.zhangwei.day38;

import android.app.Application;

import com.lidroid.xutils.DbUtils;
import com.lidroid.xutils.bitmap.BitmapGlobalConfig;
import com.lidroid.xutils.cache.MD5FileNameGenerator;
import com.qianfeng.zhangwei.day38.config.AppConfig;
import com.qianfeng.zhangwei.day38.utils.FileUtils;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-24 22:37
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class BaseApp extends Application {

    private static BaseApp app;
    private DbUtils dbUtils;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        configGlobalBitmap();
    }

    public static BaseApp getInstance() {
        return app;
    }

    /**
     * 图片加载框架全局配置
     */
    private void configGlobalBitmap() {

        BitmapGlobalConfig config = BitmapGlobalConfig.getInstance(this, FileUtils.getImageCache());
        config.setDefaultCacheExpiry(1000L * 60 * 60 * 24 * 30 * 12 * 100);
        config.setDiskCacheEnabled(true);
        //设置内存的大小 默认 4m 最小2m
        config.setMemoryCacheSize(8 * 1024 * 1024);
        config.setDiskCacheSize(1024 * 1024 * 100);
        //设置线程池的线程数量  默认 5
        config.setThreadPoolSize(4);
        //保存图片的名字以md5命名
        config.setFileNameGenerator(new MD5FileNameGenerator());
    }


    public DbUtils getDbUtils() {
        if (dbUtils == null) {
            DbUtils.DaoConfig config = new DbUtils.DaoConfig(this);
            config.setDbDir(FileUtils.getDbPath());
            config.setDbVersion(AppConfig.VERSION);
            config.setDbName(AppConfig.DB_NAME);
            //        config.setDbUpgradeListener(new DbUtils.DbUpgradeListener() {
//            @Override
//            public void onUpgrade(DbUtils dbUtils, int i, int i1) {
//
//            }
//        });
            dbUtils = DbUtils.create(config);
        }

        return dbUtils;
    }

}
