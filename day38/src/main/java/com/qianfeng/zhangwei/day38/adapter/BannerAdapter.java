package com.qianfeng.zhangwei.day38.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.lidroid.xutils.BitmapUtils;
import com.qianfeng.zhangwei.day38.R;
import com.qianfeng.zhangwei.day38.bean.Banner;
import com.qianfeng.zhangwei.day38.bean.Home;

import java.util.List;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.adapter
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 17:54
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class BannerAdapter extends AppBaseAdapter<Banner> {
    private BitmapUtils bitmapUtils;

    public BannerAdapter(Context context, List<Banner> list, BitmapUtils bitmapUtils) {
        super(context, list);
        this.bitmapUtils = bitmapUtils;
    }

    @Override
    public View getItemView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (view == null) {
            view = inflater.inflate(R.layout.item_banner_layout, viewGroup, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        bitmapUtils.display(holder.img, list.get(position).getPhoto());
        return view;
    }

    private static class ViewHolder {
        ImageView img;
        public ViewHolder(View itemView) {
            this.img = (ImageView) itemView.findViewById(R.id.item_banner_img);
        }
    }


}
