package com.qianfeng.zhangwei.day38.bean;

import com.lidroid.xutils.db.annotation.Column;
import com.lidroid.xutils.db.annotation.Id;
import com.lidroid.xutils.db.annotation.Table;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.bean
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 09:48
 * @修改人：
 * @修改时间：
 * @修改备注：
 */


@Table(name = "TAB_BANNER")
public class Banner {

    @Id
    private String id;

    @Column(column = "POSITION")
    private String position;
    private String page_url;
    private String title;
    private String price;
    private String description;
    private String type;
    private String photo;
    private String target_id;

    public void setPosition(String position) {
        this.position = position;
    }

    public void setPage_url(String page_url) {
        this.page_url = page_url;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public void setTarget_id(String target_id) {
        this.target_id = target_id;
    }

    public String getPosition() {
        return position;
    }

    public String getPage_url() {
        return page_url;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getPhoto() {
        return photo;
    }

    public String getTarget_id() {
        return target_id;
    }
}
