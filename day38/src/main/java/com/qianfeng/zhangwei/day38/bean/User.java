package com.qianfeng.zhangwei.day38.bean;

import java.io.Serializable;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.bean
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 14:22
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class User implements Serializable {

    private String uId;
    private String name;

    public User(String uId, String name) {
        this.uId = uId;
        this.name = name;
    }


    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
