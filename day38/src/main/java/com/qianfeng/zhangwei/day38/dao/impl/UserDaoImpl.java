package com.qianfeng.zhangwei.day38.dao.impl;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.qianfeng.zhangwei.day38.bean.User;
import com.qianfeng.zhangwei.day38.dao.MySqliteOpenHelper;
import com.qianfeng.zhangwei.day38.dao.UserDao;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.dao.impl
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 14:32
 * @修改人：
 * @修改时间：
 * @修改备注：
 *
 *
 *
 *
 *
 */
public class UserDaoImpl implements UserDao {
    private MySqliteOpenHelper helper;


    public UserDaoImpl() {
        this.helper = new MySqliteOpenHelper();
    }

    @Override
    public void save(User user) {
        /**
         * 区别
         * 1 getWritableDatabase 以写的方式打开数据库 如果磁盘控件已满 会想外 抛出异常
         * 2.getReadableDatabase  以读写的方式打开数据库 如果磁盘空间已满 就会以读的方式打开数据库
         */
        SQLiteDatabase database = helper.getReadableDatabase();

        try {
            database.execSQL("");
            /**
             * ：insert into table1(field1,field2) values(    value1,value2)
             */
            StringBuffer stringBuffer = new StringBuffer().append("insert into user(UID,NAME) values (").append(user.getuId()).append(",").append(user.getName()).append(" )");
            database.execSQL(stringBuffer.toString());
            //数据库操作通用代码
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (database != null) {
                database.close();
            }
        }


    }
}
