package com.qianfeng.zhangwei.day38.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;

import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridView;
import com.felipecsl.asymmetricgridview.library.widget.AsymmetricGridViewAdapter;
import com.lidroid.xutils.BitmapUtils;
import com.qianfeng.zhangwei.day38.R;
import com.qianfeng.zhangwei.day38.bean.MixProduct;

import java.util.List;

/**
 *
 * @类描述：
 * @项目名称：StudyDemo
 * @包  名： com.qianfeng.zhangwei.day38.adapter
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 22:11
 * @version v1.0
 * @修改人：
 * @修改时间：
 * @修改备注：
 */
public class AsyAdapter extends AppBaseAdapter<MixProduct> {
    BitmapUtils bitmapUtils;

    public AsyAdapter(Context context, List<MixProduct> list) {
        super(context, list);
        bitmapUtils = new BitmapUtils(context);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_asy_layout, viewGroup, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        bitmapUtils.display(holder.img,list.get(position).getPhoto());
        return convertView;
    }


    private class ViewHolder {
        ImageView img;

        public ViewHolder(View itemView) {
            this.img = (ImageView) itemView.findViewById(R.id.item_asy_img);
        }
    }
}
