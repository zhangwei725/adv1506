package com.qianfeng.zhangwei.day38.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.qianfeng.zhangwei.day38.BaseApp;
import com.qianfeng.zhangwei.day38.config.AppConfig;

/**
 * @version v1.0
 * @类描述：
 * @项目名称：StudyDemo
 * @包 名： com.qianfeng.zhangwei.day38.dao
 * @类名称：BaseAdapter
 * @创建人：张唯
 * @创建时间：2015-06-25 14:12
 * @修改人：
 * @修改时间：
 * @修改备注： 第一步 写一个类继承 SQLiteOpenHelper
 *
 * 第二步 重写构造方法  重写 onCreate  onUpgrade
 *
 * 第三步 通过Openhelper实例化 SqlDatabase对象
 * 实例化 SqlDatabase对象有两种方式
 * 区别
 * 1 getWritableDatabase 以写的方式打开数据库 如果磁盘控件已满 会想外 抛出异常
 * 2.getReadableDatabase  以读写的方式打开数据库 如果磁盘空间已满 就会以读的方式打开数据库
 * 第四步  通过sqlitedatabas 做增删改查的操作
 */
public class MySqliteOpenHelper extends SQLiteOpenHelper {
    /**
     * data accept object
     */

    public MySqliteOpenHelper() {
        super(BaseApp.getInstance(), AppConfig.DB_NAME, null, AppConfig.VERSION);
    }

    /**
     * 创建表
     *
     * @param db
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        //sql 语句

        String sql = "CREATE TABLE USER ( _ID INTEGER PRIMARY KEY AUTOINCREMENT,UID VARCHAR(20),NAME VARCHAR(20))";
        db.execSQL(sql);


    }

    /**
     * 更新的表
     *
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            db.execSQL("drop tab USER");
        }
        onCreate(db);
    }
}
