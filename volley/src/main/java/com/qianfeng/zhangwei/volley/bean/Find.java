package com.qianfeng.zhangwei.volley.bean;

import java.io.Serializable;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 16:34
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class Find implements Serializable {


    /**
     * provinceId : 3
     * themeId : 1
     * gradeId :
     * cityId : 53
     * cityName : 北京
     * provinceName : 北京
     * adviceAmount : 168
     * id : 4432
     * amount : 209
     * distance : 0
     * imgPath : 2014/01/23/2/201401231527201295405.jpg
     * lat : 39.5023082
     * sceneryAddress : 北京市朝阳区北四环东路2号顺景温泉酒店
     * lon : 116
     * status : 2
     * sceneryId : 27150
     * edit : 1
     * scenerySummary : 吉尼斯权威认证“全球最大室内温泉”
     * sceneryName : 顺景温泉
     * update_time : 2015-03-03 11:36:52
     * themeName : 温泉
     * prize : 0
     * add_time : 2013-07-23 14:43:31
     * bookFlag : 1
     * viewCount : 0
     */
    private int provinceId;
    private int themeId;
    private String gradeId;
    private int cityId;
    private String cityName;
    private String provinceName;
    private int adviceAmount;
    private int id;
    private int amount;
    private int distance;
    private String imgPath;
    private double lat;
    private String sceneryAddress;
    private int lon;
    private int status;
    private int sceneryId;
    private int edit;
    private String scenerySummary;
    private String sceneryName;
    private String update_time;
    private String themeName;
    private int prize;
    private String add_time;
    private int bookFlag;
    private int viewCount;

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public void setThemeId(int themeId) {
        this.themeId = themeId;
    }

    public void setGradeId(String gradeId) {
        this.gradeId = gradeId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public void setAdviceAmount(int adviceAmount) {
        this.adviceAmount = adviceAmount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public void setSceneryAddress(String sceneryAddress) {
        this.sceneryAddress = sceneryAddress;
    }

    public void setLon(int lon) {
        this.lon = lon;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setSceneryId(int sceneryId) {
        this.sceneryId = sceneryId;
    }

    public void setEdit(int edit) {
        this.edit = edit;
    }

    public void setScenerySummary(String scenerySummary) {
        this.scenerySummary = scenerySummary;
    }

    public void setSceneryName(String sceneryName) {
        this.sceneryName = sceneryName;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public void setBookFlag(int bookFlag) {
        this.bookFlag = bookFlag;
    }

    public void setViewCount(int viewCount) {
        this.viewCount = viewCount;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public int getThemeId() {
        return themeId;
    }

    public String getGradeId() {
        return gradeId;
    }

    public int getCityId() {
        return cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public int getAdviceAmount() {
        return adviceAmount;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public int getDistance() {
        return distance;
    }

    public String getImgPath() {
        return imgPath;
    }

    public double getLat() {
        return lat;
    }

    public String getSceneryAddress() {
        return sceneryAddress;
    }

    public int getLon() {
        return lon;
    }

    public int getStatus() {
        return status;
    }

    public int getSceneryId() {
        return sceneryId;
    }

    public int getEdit() {
        return edit;
    }

    public String getScenerySummary() {
        return scenerySummary;
    }

    public String getSceneryName() {
        return sceneryName;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public String getThemeName() {
        return themeName;
    }

    public int getPrize() {
        return prize;
    }

    public String getAdd_time() {
        return add_time;
    }

    public int getBookFlag() {
        return bookFlag;
    }

    public int getViewCount() {
        return viewCount;
    }
}
