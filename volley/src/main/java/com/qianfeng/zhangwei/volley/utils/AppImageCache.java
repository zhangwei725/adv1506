package com.qianfeng.zhangwei.volley.utils;

import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.util.LruCache;


import com.android.volley.toolbox.ImageLoader;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 11:16
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class AppImageCache implements ImageLoader.ImageCache {
    private static LruCache<String, Bitmap> lruCache;
    private static AppImageCache appImageCache;

    static {
        lruCache = new LruCache<String, Bitmap>(4 * 1024 * 1024) {

            @Override
            protected int sizeOf(String key, Bitmap value) {
                return getBitumapSize(value);
            }
        };
    }

    private AppImageCache() {

    }

    /**
     * @return
     */
    public synchronized static AppImageCache getInstance() {
        if (appImageCache == null) {
            appImageCache = new AppImageCache();
        }
        return appImageCache;
    }


    public static int getBitumapSize(Bitmap bitmap) {
        int bitmapSize = 0;
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB_MR1) {
            bitmapSize = bitmap.getByteCount();
        } else {
            bitmapSize = bitmap.getHeight() * bitmap.getRowBytes();
        }
        return bitmapSize;
    }

    /**
     * 获取图片
     *
     * @param s
     * @return
     */
    @Override
    public Bitmap getBitmap(String s) {
        return lruCache.get(s);
    }

    /**
     * 保存图片
     *
     * @param s
     * @param bitmap
     */
    @Override
    public void putBitmap(String s, Bitmap bitmap) {
        lruCache.put(s, bitmap);
    }
}
