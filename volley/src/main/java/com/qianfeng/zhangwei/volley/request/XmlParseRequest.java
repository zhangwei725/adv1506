package com.qianfeng.zhangwei.volley.request;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Package com.qianfeng.day36.http.request
 * @作 用:
 * @创 建 人: zhangwei
 * @日 期: 15-4-18 20:23
 * @修 改 人:
 * @日 期:
 */
public class XmlParseRequest extends Request<XmlPullParser> {
    private Response.Listener<XmlPullParser> mListener;
    private HashMap<String, String> parmas;
    private HashMap<String, String> heads;

    /**
     * 系统默认的构造方法
     *
     * @param method        请求方式
     * @param url           请求url
     * @param errorListener 错误请求的回调方式
     */
    private XmlParseRequest(int method, String url, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
    }

    /**
     * xml解析构造器 GET请求
     *
     * @param url           请求的url
     * @param listener      请求成功的回调接口
     * @param errorListener 请求错误的回调接口
     */
    public XmlParseRequest(String url, Response.Listener<XmlPullParser> listener, Response.ErrorListener errorListener) {
        this(Method.GET, url, errorListener);
        mListener = listener;
    }

    /**
     * @param url
     * @param params
     * @param heads
     * @param listener
     * @param errorListener
     */
    public XmlParseRequest(String url, HashMap<String, String> params, HashMap<String, String> heads, Response.Listener<XmlPullParser> listener, Response.ErrorListener errorListener) {
        this(Method.POST, url, errorListener);
        this.parmas = params;
        this.heads = heads;
    }


    public XmlParseRequest(String url, HashMap<String, String> params, Response.Listener<XmlPullParser> listener, Response.ErrorListener errorListener) {
        this(url, params, null, listener, errorListener);
    }

    /**
     * post请求提交head参数
     *
     * @return
     * @throws AuthFailureError
     */
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return heads != null && !heads.isEmpty() ? heads : super.getHeaders();
    }

    /**
     * POST请求提交参数
     *
     * @return
     * @throws AuthFailureError
     */
    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return parmas != null && !parmas.isEmpty() ? parmas : super.getParams();
    }

    @Override
    protected Response<XmlPullParser> parseNetworkResponse(NetworkResponse response) {
        try {
            String data = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            XmlPullParserFactory parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser xmlPullParser = parserFactory.newPullParser();
            xmlPullParser.setInput(new StringReader(data));
            Response.success(xmlPullParser, HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.error(new VolleyError(e));
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            return Response.error(new VolleyError(e));
        }
        return null;
    }

    @Override
    protected void deliverResponse(XmlPullParser response) {
        mListener.onResponse(response);
    }

}
