package com.qianfeng.zhangwei.volley;

import android.app.Application;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 16:14
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class BaseApplication extends Application {

    private static BaseApplication application;


    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }


    public static BaseApplication getApplication() {
        return application;
    }
}
