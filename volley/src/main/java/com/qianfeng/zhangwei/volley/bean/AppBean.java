package com.qianfeng.zhangwei.volley.bean;

import java.io.Serializable;
import java.util.List;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 16:38
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 * <p>
 * page_total: 0,
 * pagenumber: 3,
 */
public class AppBean implements Serializable {
    private int status;
    private AppendInfo append_info;
    private List<Find> result;
    private String msg;
    private int page_total;
    private int pagenumber;
    private int result_count;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public AppendInfo getAppend_info() {
        return append_info;
    }

    public void setAppend_info(AppendInfo append_info) {
        this.append_info = append_info;
    }

    public List<Find> getResult() {
        return result;
    }

    public void setResult(List<Find> result) {
        this.result = result;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getPage_total() {
        return page_total;
    }

    public void setPage_total(int page_total) {
        this.page_total = page_total;
    }

    public int getPagenumber() {
        return pagenumber;
    }

    public void setPagenumber(int pagenumber) {
        this.pagenumber = pagenumber;
    }

    public int getResult_count() {
        return result_count;
    }

    public void setResult_count(int result_count) {
        this.result_count = result_count;
    }
}
