package com.qianfeng.zhangwei.volley.bean;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 16:39
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class AppendInfo {


    /**
     * provinceId : 3
     * provinceName : 北京
     */
    private int provinceId;
    private String provinceName;

    public void setProvinceId(int provinceId) {
        this.provinceId = provinceId;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public int getProvinceId() {
        return provinceId;
    }

    public String getProvinceName() {
        return provinceName;
    }
}
