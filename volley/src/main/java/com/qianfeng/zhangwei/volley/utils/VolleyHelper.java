package com.qianfeng.zhangwei.volley.utils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.qianfeng.zhangwei.volley.BaseApplication;
import com.qianfeng.zhangwei.volley.R;

import java.util.Objects;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 16:08
 * 修 改 人：
 * 日   期：
 * 版 本 号：v
 */
public class VolleyHelper {
    private static VolleyHelper helper;
    private RequestQueue queue;
    private ImageLoader imageLoader;

    private VolleyHelper() {
        getQueue();
    }

    public static VolleyHelper getInstance() {
        if (helper == null) {
            helper = new VolleyHelper();
        }
        return helper;
    }

    public RequestQueue getQueue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(BaseApplication.getApplication());
        }
        return queue;
    }


    public ImageLoader getImageLoader() {
        if (imageLoader == null) {
            imageLoader = new ImageLoader(getQueue(), AppImageCache.getInstance());
        }
        return imageLoader;
    }

    public void addRequest(Request<?> request) {
        getQueue().add(request);
    }

    public void cancelAll(Object tag) {
        getQueue().cancelAll(tag);
    }


}
