package com.qianfeng.zhangwei.volley.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 16:31
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public abstract  class AppBaseAdapter<T> extends BaseAdapter {
    public List<T> list;

    public Context context;


    public LayoutInflater inflater;

    public AppBaseAdapter(List<T> list, Context context) {
        this.list = list;
        this.context = context;
        inflater = LayoutInflater.from(context);


    }

    @Override
    public int getCount() {
        return list != null && !list.isEmpty() ? list.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getItemView(position,convertView,parent);
    }

    public abstract  View getItemView(int position, View convertView, ViewGroup parent);
}
