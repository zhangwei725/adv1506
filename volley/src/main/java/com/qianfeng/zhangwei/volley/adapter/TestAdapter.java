package com.qianfeng.zhangwei.volley.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.qianfeng.zhangwei.volley.utils.AppImageCache;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 11:50
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class TestAdapter extends BaseAdapter {

    private ImageLoader loader;

    public TestAdapter(RequestQueue queue) {
        loader = new ImageLoader(queue, AppImageCache.getInstance());
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        NetworkImageView networkImageView = null;

        networkImageView.setImageUrl("url", loader);
        return null;
    }
}
