package com.qianfeng.zhangwei.volley.smaple;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.qianfeng.zhangwei.volley.R;
import com.qianfeng.zhangwei.volley.adapter.FindAdapter;
import com.qianfeng.zhangwei.volley.bean.AppBean;
import com.qianfeng.zhangwei.volley.bean.Find;
import com.qianfeng.zhangwei.volley.request.FastJsonRequest;
import com.qianfeng.zhangwei.volley.utils.UrlConstants;
import com.qianfeng.zhangwei.volley.utils.VolleyHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Volley框架案例
 */
public class VolleyActivity extends AppCompatActivity {
    private ListView listView;
    private List<Find> list = new ArrayList<Find>();

    private FindAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_volley);
        initView();
        loadData();

    }


    private void loadData() {
        FastJsonRequest<AppBean> request = new FastJsonRequest<AppBean>(UrlConstants.url, AppBean.class, new Response.Listener<AppBean>() {
            @Override
            public void onResponse(AppBean appBean) {
                if (appBean != null && appBean.getStatus() == 200) {
                    list.addAll(appBean.getResult());
                    adapter.notifyDataSetChanged();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        request.setTag(this);
        VolleyHelper.getInstance().addRequest(request);
    }

    private void initView() {
        listView = (ListView) this.findViewById(R.id.listview);
        adapter = new FindAdapter(list, this);
        listView.setAdapter(adapter);
    }


    @Override
    protected void onStop() {
        super.onStop();
        VolleyHelper.getInstance().cancelAll(this);
    }
}
