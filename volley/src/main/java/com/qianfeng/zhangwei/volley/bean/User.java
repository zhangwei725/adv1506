package com.qianfeng.zhangwei.volley.bean;

import java.io.Serializable;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 10:51
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class User implements Serializable {

    private String id;
    private String name;

    public User() {
    }

    public User(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
