package com.qianfeng.zhangwei.volley.request;

import com.alibaba.fastjson.JSONObject;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * @Package com.qianfeng.day36.http.request
 * @作 用: 自定FastJson请求 需要导入第三方fasjson
 * @创 建 人: zhangwei
 * @日 期: 15-4-18 18:32
 * @修 改 人:
 * @日 期:
 */
public class FastJsonRequest<T> extends Request<T> {
    private Class<T> mCls;
    private Response.Listener<T> listener;
    private Map<String, String> heads;
    private Map<String, String> mParams;

    /**
     * @param method        请求方式
     * @param url           请求url
     * @param heads         请求头部参数
     * @param params        Post请求参数
     * @param cls           解析类
     * @param listener      请求成功回调
     * @param errorListener 请求失败回调
     */
    public FastJsonRequest(int method, String url, Map<String, String> heads, Map<String, String> params, Class<T> cls, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.mCls = cls;
        this.listener = listener;
        this.heads = heads;
        this.mParams = params;
    }

    /**
     * @param method        请求方式
     * @param url           请求url
     * @param params        Post请求参数
     * @param cls           解析类
     * @param listener      请求成功回调
     * @param errorListener 请求失败回调
     */
    public FastJsonRequest(int method, String url, Map<String, String> params, Class<T> cls, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(method, url, null, params, cls, listener, errorListener);
    }

    /**
     * @param method        请求方式
     * @param url           请求url
     * @param cls           解析类
     * @param listener      请求成功回调
     * @param errorListener 请求失败回调
     */
    public FastJsonRequest(int method, String url, Class<T> cls, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(method, url, null, null, cls, listener, errorListener);
    }

    /**
     * 默认get请求数据
     *
     * @param url           请求url
     * @param cls           解析类
     * @param listener      请求成功回调
     * @param errorListener 请求失败回调
     */
    public FastJsonRequest(String url, Class<T> cls, Response.Listener listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.mCls = cls;
        this.listener = listener;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return mParams != null && !mParams.isEmpty() ? mParams : super.getParams();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return heads != null && !heads.isEmpty() ? heads : super.getHeaders();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse networkResponse) {
        String charset = HttpHeaderParser.parseCharset(networkResponse.headers);
        try {
            String data = new String(networkResponse.data, charset);
            T t = JSONObject.parseObject(data, mCls);
            return Response.success(t, HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return Response.error(new VolleyError(e));
        }
    }
    @Override
    protected void deliverResponse(T t) {
        listener.onResponse(t);
    }
}
