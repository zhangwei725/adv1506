package com.qianfeng.zhangwei.volley.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.qianfeng.zhangwei.volley.R;
import com.qianfeng.zhangwei.volley.bean.Find;
import com.qianfeng.zhangwei.volley.utils.VolleyHelper;

import java.util.List;

/**
 * 描   述：
 * 创 建 人：zhangwei
 * 日    期：2015-06-23 16:34
 * 修 改 人：
 * 日   期：
 * 版 本 号：v1.0
 */
public class FindAdapter extends AppBaseAdapter<Find> {
    public FindAdapter(List<Find> list, Context context) {
        super(list, context);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup parent) {
        ViewHodler vh = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.item_find_layout, parent, false);
            vh = new ViewHodler(convertView);
            convertView.setTag(vh);
        } else {
            vh = (ViewHodler) convertView.getTag();
        }
        vh.img.setErrorImageResId(R.mipmap.ic_launcher);
        vh.img.setDefaultImageResId(R.mipmap.ic_launcher);
        vh.img.setImageUrl("http://upload.17u.com/uploadfile/scenerypic_common/300_225/" + list.get(position).getImgPath(), VolleyHelper.getInstance().getImageLoader());
        return convertView;
    }

    private static class ViewHodler {
        NetworkImageView img;
        TextView text;

        public ViewHodler(View itemView) {
            this.img = (NetworkImageView) itemView.findViewById(R.id.item_img);
            this.text = (TextView) itemView.findViewById(R.id.item_text);
        }
    }


}
