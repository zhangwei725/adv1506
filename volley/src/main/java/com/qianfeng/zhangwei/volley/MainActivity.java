package com.qianfeng.zhangwei.volley;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSONObject;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.qianfeng.zhangwei.volley.bean.User;
import com.qianfeng.zhangwei.volley.request.FastJsonRequest;
import com.qianfeng.zhangwei.volley.utils.AppImageCache;
import com.qianfeng.zhangwei.volley.utils.VolleyHelper;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Handler;


/**
 * 简介 Volley
 * 1,json数据加载,图片异步下载
 * 2.缓存功能
 * 3.跟Activity的生命周期联动
 * 第一大模块   json数据加载  以前连接网络都是使用 urlConnection
 * httpClient类联网
 * 数据加载
 * StringRquest
 * JsonArrayRequest
 * JsonObjctRequest
 * 图片加载
 * ImageRequest
 * Imageloader
 * NetWorkImagView
 */
public class MainActivity extends AppCompatActivity {
    private RequestQueue queue;
    private String url;

    private String imageUrl = "http://p3.so.qhimg.com/t015121fd488c2eb9d3.jpg";
    ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        queue = Volley.newRequestQueue(this);
        img = (ImageView) MainActivity.this.findViewById(R.id.img);
//        stringRequestMethod();
//        jsonObjectRequestMethod();
//        imageRequestMethod();
        imageLoaderMethod();

    }

    /**
     * StringRequest的基本使用
     */
    private void stringRequestMethod() {
        //第一步创建请求队列

        //第二步创建请求
        /**
         * 参数说明
         *
         * 参数一 请求方式
         * 参数二  请求网络地址
         * 参数三  请求数据成功的回调接口
         * 参数四  请求错误的回调接口
         *
         */
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                User user = JSONObject.parseObject(s, User.class);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> map = new HashMap<>();
                return map;
            }
        };


        //启用本地缓存
//        request.setShouldCache(true);     可有可无
        //第三步 设置标记  作用用来取消请求
        request.setTag("1");
        //第四步添加至请求队列
        VolleyHelper.getInstance().addRequest(request);
    }


    private void jsonObjectRequestMethod() {
        User user = new User("1", "hehe");
        String jsonStringBody = JSONObject.toJSONString(user);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonStringBody, new Response.Listener<org.json.JSONObject>() {
            @Override
            public void onResponse(org.json.JSONObject jsonObject) {
                System.out.print("测试");

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        jsonObjectRequest.setTag(this);
        queue.add(jsonObjectRequest);


    }

    /**
     * 适合单张的下载
     * String url,
     * Listener<Bitmap> listener,
     * int maxWidth,
     * int maxHeight,
     * ScaleType scaleType,
     * Config decodeConfig,
     * ErrorListener errorListener
     */
    private void imageRequestMethod() {
        ImageRequest imageRequest = new ImageRequest(imageUrl, new Response.Listener<Bitmap>() {
            @Override
            public void onResponse(Bitmap bitmap) {

                img.setImageBitmap(bitmap);
            }
        }, 300, 300, ImageView.ScaleType.FIT_XY, Bitmap.Config.RGB_565, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                String msg = volleyError.getMessage();
            }
        });
        queue.add(imageRequest);
    }

    /**
     * 第一步初始化ImageLoader
     * 第二步 获取请求的listener
     */
    public void imageLoaderMethod() {
        AppImageCache cache = AppImageCache.getInstance();
        /**
         * 参数一  请求队列
         * 参数, 图片处理接口  注意:框架 只提供了接口 未实现
         */
        ImageLoader imageLoader = new ImageLoader(queue, cache);
        //第二步 获取请求的listener
        ImageLoader.ImageListener listener = ImageLoader.getImageListener(img, R.mipmap.ic_launcher, R.mipmap.ic_launcher);
        //第三部 调用imageload   get方法;
        imageLoader.get(imageUrl, listener, 300, 300, ImageView.ScaleType.FIT_XY);
    }

    /**
     * NetwrokImageView的使用
     */
    private void netWorkImageView() {
        AppImageCache cache = AppImageCache.getInstance();
        /**
         * 参数一  请求队列
         * 参数, 图片处理接口  注意:框架 只提供了接口 未实现
         */
        ImageLoader imageLoader = new ImageLoader(queue, cache);
        NetworkImageView nwImg = (NetworkImageView) this.findViewById(R.id.networkImageView);
        nwImg.setDefaultImageResId(R.mipmap.ic_launcher);
        nwImg.setErrorImageResId(R.mipmap.ic_launcher);
        nwImg.setImageUrl(imageUrl, imageLoader);
    }


    private void fastJsonRequest() {
        FastJsonRequest<User> request = new FastJsonRequest<>(url, User.class, new Response.Listener<User>() {

            @Override
            public void onResponse(User user) {


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        queue.add(request);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        VolleyHelper.getInstance().cancelAll(this);
    }


}
