package com.qianfeng.zhangwei.volley.request;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * @Package com.qianfeng.day36.http
 * @作 用: 自定义GsonRequest请求
 * @创 建 人: zhangwei
 * @日 期: 15-4-18 17:20
 * @修 改 人:
 * @日 期:
 */
public class GsonRequest<T> extends Request<T> {
    /**
     * 请求头部信息
     */
    private Map<String, String> headers;
    /**
     * 解析类型
     */
    private Class<T> cls;
    /**
     * 请求成功接口回调
     */
    private Response.Listener<T> listener;
    /**
     * 提交参数
     */
    private Map<String, String> mParams;

    private Gson gson = new Gson();

    /**
     * @param method        请求的方式
     * @param url           请求的url
     * @param cls           需要解析的对象
     * @param params        参数
     * @param headers       添加头部信息
     * @param listener      数据请求成功回调
     * @param errorListener 错误请求回调
     */
    public GsonRequest(int method, String url, Class<T> cls, Map<String, String> params, Map<String, String> headers, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.cls = cls;
        this.mParams = params;
        this.headers = headers;
        this.listener = listener;
    }

    /**
     * 构造函数
     *
     * @param method        请求的方式
     * @param url           请求的url
     * @param cls           需要解析的对象
     * @param mParams       添加头部信息
     * @param listener      数据请求成功回调
     * @param errorListener 错误请求回调
     */
    public GsonRequest(int method, String url, Class<T> cls, Map<String, String> mParams, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(method, url, cls, mParams, null, listener, errorListener);
    }

    public GsonRequest(int method, String url, Class<T> cls, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        this(method, url, cls, null, null, listener, errorListener);
    }
    /**
     * 默认GET请求
     *
     * @param url
     * @param listener
     * @param errorListener
     */
    public GsonRequest(String url, Class<T> cls, Response.Listener<T> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);
        this.listener = listener;
        this.cls = cls;
    }

    /**
     * 服务器请求头部
     *
     * @return
     */
    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return headers != null && !headers.isEmpty()? headers : super.getHeaders();
    }

    /**
     * 通过post请求
     *
     * @return
     * @throws AuthFailureError
     */
    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return mParams != null && !mParams.isEmpty() ? mParams : super.getParams();
    }

    /**
     * @param networkResponse
     * @return
     */
    @Override
    protected Response parseNetworkResponse(NetworkResponse networkResponse) {
        String charset = HttpHeaderParser.parseCharset(networkResponse.headers);
        try {
            String data = new String(networkResponse.data, charset);
            return Response.success(gson.fromJson(data, cls), HttpHeaderParser.parseCacheHeaders(networkResponse));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new VolleyError(e));
        }
    }

    /**
     * @param response
     */
    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }
}
